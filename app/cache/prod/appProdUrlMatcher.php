<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appProdUrlMatcher
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        // crowdrise_membre_homepage
        if ($pathinfo === '/accueil') {
            return array (  '_controller' => 'Crowdrise\\MembreBundle\\Controller\\DefaultController::indexAction',  '_route' => 'crowdrise_membre_homepage',);
        }

        // crowdrise_membre_loginpage
        if ($pathinfo === '/login') {
            return array (  '_controller' => 'Crowdrise\\MembreBundle\\Controller\\DefaultController::loginAction',  '_route' => 'crowdrise_membre_loginpage',);
        }

        // crowdrise_membre_registerpage
        if ($pathinfo === '/register') {
            return array (  '_controller' => 'Crowdrise\\MembreBundle\\Controller\\DefaultController::registerAction',  '_route' => 'crowdrise_membre_registerpage',);
        }

        // crowdrise_problemes_homepage
        if ($pathinfo === '/stackprobs') {
            return array (  '_controller' => 'Crowdrise\\ProblemesBundle\\Controller\\DefaultController::problemsAction',  '_route' => 'crowdrise_problemes_homepage',);
        }

        // crowdrise_profile_homepage
        if ($pathinfo === '/profile') {
            return array (  '_controller' => 'Crowdrise\\ProfileBundle\\Controller\\DefaultController::profileAction',  '_route' => 'crowdrise_profile_homepage',);
        }

        // crowdrise_idees_homepage
        if ($pathinfo === '/ideas') {
            return array (  '_controller' => 'Crowdrise\\IdeesBundle\\Controller\\DefaultController::someideasAction',  '_route' => 'crowdrise_idees_homepage',);
        }

        // crowdrise_allidees_homepage
        if ($pathinfo === '/allIdeas') {
            return array (  '_controller' => 'Crowdrise\\IdeesBundle\\Controller\\DefaultController::allideasAction',  '_route' => 'crowdrise_allidees_homepage',);
        }

        // crowdrise_projets_homepage
        if ($pathinfo === '/projects') {
            return array (  '_controller' => 'Crowdrise\\ProjetsBundle\\Controller\\DefaultController::projectsAction',  '_route' => 'crowdrise_projets_homepage',);
        }

        if (0 === strpos($pathinfo, '/a')) {
            // crowdrise_allprojets_homepage
            if ($pathinfo === '/allProjects') {
                return array (  '_controller' => 'Crowdrise\\ProjetsBundle\\Controller\\DefaultController::allprojectsAction',  '_route' => 'crowdrise_allprojets_homepage',);
            }

            if (0 === strpos($pathinfo, '/administration_')) {
                // crowdrise_administration_homepage
                if ($pathinfo === '/administration_home') {
                    return array (  '_controller' => 'Crowdrise\\AdministrationBundle\\Controller\\DefaultController::administrationAction',  '_route' => 'crowdrise_administration_homepage',);
                }

                // crowdrise_administration_users
                if ($pathinfo === '/administration_users') {
                    return array (  '_controller' => 'Crowdrise\\AdministrationBundle\\Controller\\UtilisateurController::affichageUtilisateursAction',  '_route' => 'crowdrise_administration_users',);
                }

            }

        }

        // crowdrise_administration_delete_user
        if (0 === strpos($pathinfo, '/deleteuser') && preg_match('#^/deleteuser/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'crowdrise_administration_delete_user')), array (  '_controller' => 'Crowdrise\\AdministrationBundle\\Controller\\UtilisateurController::suppUtilisateurAction',));
        }

        if (0 === strpos($pathinfo, '/a')) {
            // crowdrise_administration_ideas_requests
            if ($pathinfo === '/administration_ideasrequests') {
                return array (  '_controller' => 'Crowdrise\\AdministrationBundle\\Controller\\IdeeController::affichageIdeesAction',  '_route' => 'crowdrise_administration_ideas_requests',);
            }

            // crowdrise_administration_accept_idea
            if (0 === strpos($pathinfo, '/acceptidea') && preg_match('#^/acceptidea/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'crowdrise_administration_accept_idea')), array (  '_controller' => 'Crowdrise\\AdministrationBundle\\Controller\\IdeeController::acceptIdeeAction',));
            }

        }

        // crowdrise_administration_refuse_idea
        if (0 === strpos($pathinfo, '/refuseidea') && preg_match('#^/refuseidea/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'crowdrise_administration_refuse_idea')), array (  '_controller' => 'Crowdrise\\AdministrationBundle\\Controller\\IdeeController::refuseIdeaAction',));
        }

        // crowdrise_administration_claims
        if ($pathinfo === '/administration_claims') {
            return array (  '_controller' => 'Crowdrise\\AdministrationBundle\\Controller\\ReclamationController::affichageReclamationsAction',  '_route' => 'crowdrise_administration_claims',);
        }

        // crowdrise_administration_delete_claim
        if (0 === strpos($pathinfo, '/deleteclaim') && preg_match('#^/deleteclaim/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'crowdrise_administration_delete_claim')), array (  '_controller' => 'Crowdrise\\AdministrationBundle\\Controller\\ReclamationController::suppReclamationAction',));
        }

        // crowdrise_administration_projects
        if ($pathinfo === '/administration_projects') {
            return array (  '_controller' => 'Crowdrise\\AdministrationBundle\\Controller\\ProjetController::affichageProjetsAction',  '_route' => 'crowdrise_administration_projects',);
        }

        // crowdrise_administration_delete_project
        if (0 === strpos($pathinfo, '/deleteproject') && preg_match('#^/deleteproject/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'crowdrise_administration_delete_project')), array (  '_controller' => 'Crowdrise\\AdministrationBundle\\Controller\\ProjetController::suppProjetAction',));
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
