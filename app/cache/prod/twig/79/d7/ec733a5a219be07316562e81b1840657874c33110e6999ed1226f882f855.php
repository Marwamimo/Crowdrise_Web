<?php

/* CrowdriseAdministrationBundle::layout.html.twig */
class __TwigTemplate_79d7ec733a5a219be07316562e81b1840657874c33110e6999ed1226f882f855 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'navbar' => array($this, 'block_navbar'),
            'nav_li_dash' => array($this, 'block_nav_li_dash'),
            'nav_li_users' => array($this, 'block_nav_li_users'),
            'nav_li_ideasreq' => array($this, 'block_nav_li_ideasreq'),
            'nav_li_projects' => array($this, 'block_nav_li_projects'),
            'nav_li_claims' => array($this, 'block_nav_li_claims'),
            'nav_li_statistics' => array($this, 'block_nav_li_statistics'),
            'pagecontent' => array($this, 'block_pagecontent'),
            'pageheader' => array($this, 'block_pageheader'),
            'breadcrumb' => array($this, 'block_breadcrumb'),
            'nbusers' => array($this, 'block_nbusers'),
            'nbideas' => array($this, 'block_nbideas'),
            'nbprojects' => array($this, 'block_nbprojects'),
            'nbclaims' => array($this, 'block_nbclaims'),
            'bundleext_pie' => array($this, 'block_bundleext_pie'),
            'recentidasactivity' => array($this, 'block_recentidasactivity'),
            'recentclaimsactivity' => array($this, 'block_recentclaimsactivity'),
            'recentprojectsactivity' => array($this, 'block_recentprojectsactivity'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
    
";
        // line 4
        $this->displayBlock('head', $context, $blocks);
        // line 57
        echo "
<body class=\"sidebar-wide\">


";
        // line 61
        $this->displayBlock('navbar', $context, $blocks);
        // line 79
        echo "


    
<!-- Page container -->
<div class=\"page-container\">
  <!-- Sidebar -->
  <div class=\"sidebar collapse\">
    <div class=\"sidebar-content\">




      <!-- User dropdown -->
      <div class=\"user-menu dropdown\"><a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\"><img src=\"";
        // line 93
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/images/demo/users/face1.png"), "html", null, true);
        echo "\" alt=\"\">
        <div class=\"user-info\">Oussama Aydi <span>Administrator</span></div>
        </a>


        <div class=\"popup dropdown-menu dropdown-menu-right\">
          <div class=\"thumbnail\">
            <div class=\"thumb\"><img alt=\"\" src=\"";
        // line 100
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/images/demo/users/face1.png"), "html", null, true);
        echo "\">
              <div class=\"thumb-options\"><span><a href=\"#\" class=\"btn btn-icon btn-success\"><i class=\"icon-pencil\"></i></a><a href=\"#\" class=\"btn btn-icon btn-success\"><i class=\"icon-remove\"></i></a></span></div>
            </div>
            <div class=\"caption text-center\">
              <h6>Oussama Aydi <small>Administrator</small></h6>
            </div>
          </div>
  
        </div>
      </div>
      <!-- /user dropdown -->



      <!-- Main navigation -->
      <ul class=\"navigation\">
          
       ";
        // line 117
        $this->displayBlock('nav_li_dash', $context, $blocks);
        // line 120
        echo "       
       ";
        // line 121
        $this->displayBlock('nav_li_users', $context, $blocks);
        // line 124
        echo "       
       ";
        // line 125
        $this->displayBlock('nav_li_ideasreq', $context, $blocks);
        // line 128
        echo "       
       ";
        // line 129
        $this->displayBlock('nav_li_projects', $context, $blocks);
        // line 132
        echo "       
       ";
        // line 133
        $this->displayBlock('nav_li_claims', $context, $blocks);
        // line 136
        echo "  
       ";
        // line 137
        $this->displayBlock('nav_li_statistics', $context, $blocks);
        // line 140
        echo "       
      </ul>
      <!-- /main navigation -->



    </div>
  </div>
  <!-- /sidebar -->



  
";
        // line 153
        $this->displayBlock('pagecontent', $context, $blocks);
        // line 341
        echo "  
  
</div>
<!-- /page container -->

    
</body>
</html>";
    }

    // line 4
    public function block_head($context, array $blocks = array())
    {
        // line 5
        echo "
<head>
<meta charset=\"utf-8\">
<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
<title>Crowdrise Admin</title>



<link href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\">
<link href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/css/londinium-theme.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\">
<link href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/css/styles.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\">
<link href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/css/icons.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\">


<link href=\"http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext\" rel=\"stylesheet\" type=\"text/css\">


<script type=\"text/javascript\" src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js\"></script>
<script type=\"text/javascript\" src=\"http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js\"></script>

<script type=\"text/javascript\" src=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/charts/sparkline.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/forms/uniform.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/forms/select2.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/forms/inputmask.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/forms/autosize.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/forms/inputlimit.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/forms/listbox.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/forms/multiselect.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/forms/validate.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/forms/tags.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/forms/switch.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/forms/uploader/plupload.full.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/forms/uploader/plupload.queue.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/forms/wysihtml5/wysihtml5.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/forms/wysihtml5/toolbar.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/interface/daterangepicker.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/interface/fancybox.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/interface/moment.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/interface/jgrowl.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/interface/datatables.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/interface/colorpicker.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/interface/fullcalendar.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 48
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/interface/timepicker.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 49
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/interface/collapsible.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 51
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/application.js"), "html", null, true);
        echo "\"></script>


</head>

";
    }

    // line 61
    public function block_navbar($context, array $blocks = array())
    {
        // line 62
        echo "
<!-- Navbar -->
<div class=\"navbar navbar-inverse\" role=\"navigation\">
  <div class=\"navbar-header\"><a class=\"navbar-brand\" href=\"";
        // line 65
        echo $this->env->getExtension('routing')->getPath("crowdrise_administration_homepage");
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/images/logo.png"), "html", null, true);
        echo "\" alt=\"Londinium\"></a><a class=\"sidebar-toggle\"><i class=\"icon-paragraph-justify2\"></i></a>
    <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#navbar-icons\"><span class=\"sr-only\">Toggle navbar</span><i class=\"icon-grid3\"></i></button>
    <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".sidebar\"><span class=\"sr-only\">Toggle navigation</span><i class=\"icon-paragraph-justify2\"></i></button>
  </div>
  <ul class=\"nav navbar-nav navbar-right collapse\" id=\"navbar-icons\">

    <li><a href=\"#\"><i class=\"icon-exit\"></i> Logout</a>
    </li>
  </ul>
</div>


<!-- /navbar -->
";
    }

    // line 117
    public function block_nav_li_dash($context, array $blocks = array())
    {
        // line 118
        echo "        <li class=\"active\"><a href=\"";
        echo $this->env->getExtension('routing')->getPath("crowdrise_administration_homepage");
        echo "\"><span>Dashboard</span> <i class=\"icon-screen2\"></i></a></li>
       ";
    }

    // line 121
    public function block_nav_li_users($context, array $blocks = array())
    {
        // line 122
        echo "        <li><a href=\"";
        echo $this->env->getExtension('routing')->getPath("crowdrise_administration_users");
        echo "\"><span>Users</span> <i class=\"icon-users2\"></i></a></li>
        ";
    }

    // line 125
    public function block_nav_li_ideasreq($context, array $blocks = array())
    {
        // line 126
        echo "       <li><a href=\"";
        echo $this->env->getExtension('routing')->getPath("crowdrise_administration_ideas_requests");
        echo "\"><span>Ideas Requests</span> <i class=\"icon-stack\"></i></a> </li>
       ";
    }

    // line 129
    public function block_nav_li_projects($context, array $blocks = array())
    {
        // line 130
        echo "       <li><a href=\"";
        echo $this->env->getExtension('routing')->getPath("crowdrise_administration_projects");
        echo "\"><span>Projects</span> <i class=\"icon-stackoverflow\"></i></a> </li>
       ";
    }

    // line 133
    public function block_nav_li_claims($context, array $blocks = array())
    {
        // line 134
        echo "        <li><a href=\"";
        echo $this->env->getExtension('routing')->getPath("crowdrise_administration_claims");
        echo "\"><span>Claims</span> <i class=\"icon-bubble6\"></i></a> </li>
       ";
    }

    // line 137
    public function block_nav_li_statistics($context, array $blocks = array())
    {
        // line 138
        echo "        <li><a href=\"statistics.html\"><span>Statistics</span> <i class=\"icon-bars\"></i></a></li>
        ";
    }

    // line 153
    public function block_pagecontent($context, array $blocks = array())
    {
        // line 154
        echo "  <!-- Page content -->
  <div class=\"page-content\">
      
   ";
        // line 157
        $this->displayBlock('pageheader', $context, $blocks);
        // line 173
        echo "
    
    ";
        // line 175
        $this->displayBlock('breadcrumb', $context, $blocks);
        // line 187
        echo "

     <!-- Page statistics -->
    <ul class=\"page-stats list-justified\">
      <li class=\"bg-primary\">
        <div class=\"page-stats-showcase\"> <span>No. of Users</span>
          ";
        // line 193
        $this->displayBlock('nbusers', $context, $blocks);
        // line 194
        echo "        </div>
        <div class=\"bar-default chart\">10,14,8,45,23,41,22,31,19,12, 28, 21, 24, 20</div>
      </li>


      <li class=\"bg-danger\">
        <div class=\"page-stats-showcase\"> <span>No. of Ideas</span>
          ";
        // line 201
        $this->displayBlock('nbideas', $context, $blocks);
        // line 202
        echo "        </div>
        <div class=\"bar-default chart\">10,14,8,45,23,41,22,31,19,12, 28, 21, 24, 20</div>
      </li>


      <li class=\"bg-success\">
        <div class=\"page-stats-showcase\"> <span>No. of Projects</span>
          ";
        // line 209
        $this->displayBlock('nbprojects', $context, $blocks);
        // line 210
        echo "        </div>
        <div class=\"bar-default chart\">10,14,8,45,23,41,22,31,19,12, 28, 21, 24, 20</div>
      </li>


      <li class=\"bg-info\">
        <div class=\"page-stats-showcase\"> <span>No. of Claims</span>
          ";
        // line 217
        $this->displayBlock('nbclaims', $context, $blocks);
        // line 218
        echo "        </div>
        <div class=\"bar-default chart\">10,14,8,45,23,41,22,31,19,12, 28, 21, 24, 20</div>
      </li>


    </ul>
    <!-- /page statistics -->

    
     <!-- Pie Bundle Externe -->
     
        <div class=\"panel panel-default\">
          <div class=\"panel-heading\">
            <h6 class=\"panel-title\"><i class=\"icon-calendar2\"></i> Piedddddddd</h6>
          </div>
          <div class=\"panel-body\">
             
            ";
        // line 235
        $this->displayBlock('bundleext_pie', $context, $blocks);
        // line 236
        echo "            
          </div>
        </div>
     
      <!-- /pie Bundle Externe -->
        
        

    <!-- Page tabs -->
    <div class=\"tabbable page-tabs\">
      <ul class=\"nav nav-tabs\">
        <li class=\"active\"><a href=\"#activity\" data-toggle=\"tab\"><i class=\"icon-file\"></i> Activity</a></li>
     
      </ul>



      <div class=\"tab-content\">


        <!-- First tab -->


        <div class=\"tab-pane active fade in\" id=\"activity\">
 
          <!-- Recent activity -->
          <div class=\"block\">
            <h6 class=\"heading-hr\"><i class=\"icon-file\"></i> Recent Ideas activity</h6>


            <ul class=\"media-list\">

                ";
        // line 268
        $this->displayBlock('recentidasactivity', $context, $blocks);
        // line 277
        echo "

            </ul>
          </div>
                
          
          <div class=\"block\">
            <h6 class=\"heading-hr\"><i class=\"icon-file\"></i> Recent Claims activity</h6>


            <ul class=\"media-list\">

             ";
        // line 289
        $this->displayBlock('recentclaimsactivity', $context, $blocks);
        // line 299
        echo "
            </ul>
          </div>
               
   <!-- /recent project activity 
         <div class=\"block\">
            <h6 class=\"heading-hr\"><i class=\"icon-file\"></i> Recent Projects activity</h6>


            <ul class=\"media-list\">

           ";
        // line 310
        $this->displayBlock('recentprojectsactivity', $context, $blocks);
        // line 320
        echo "
            </ul>
          </div>      -->
                
          <!-- /recent activity -->
        </div>
        <!-- /first tab -->


      </div>
    </div>
    <!-- /page tabs -->
    <!-- Footer -->
    <div class=\"footer clearfix\">
      <div class=\"pull-left\">&copy; 2016. Crowdrise Admin by <a href=\"http://themeforest.net/user/Kopyov\">DevX Team</a></div>
      <div class=\"pull-right icons-group\"> <a href=\"#\"><i class=\"icon-screen2\"></i></a> <a href=\"#\"><i class=\"icon-balance\"></i></a> <a href=\"#\"><i class=\"icon-cog3\"></i></a> </div>
    </div>
    <!-- /footer -->
  </div>
  <!-- /page content -->
  ";
    }

    // line 157
    public function block_pageheader($context, array $blocks = array())
    {
        echo "   
       
    <!-- Page header -->
    <div class=\"page-header\">
      <div class=\"page-title\">
        <h3>Dashboard <small>Welcome Administrator. This is your managment space.</small></h3>
      </div>
      <div id=\"reportrange\" class=\"range\">
        <div class=\"visible-xs header-element-toggle\"><a class=\"btn btn-primary btn-icon\"><i class=\"icon-calendar\"></i></a></div>
        <div class=\"date-range\"></div>
        <span class=\"label label-danger\">9</span></div>
    </div>


    <!-- /page header -->
    ";
    }

    // line 175
    public function block_breadcrumb($context, array $blocks = array())
    {
        // line 176
        echo "    <!-- Breadcrumbs line -->
    <div class=\"breadcrumb-line\">
      <ul class=\"breadcrumb\">
        <li><a href=\"index.html\">Home</a></li>
        <li class=\"active\">Dashboard</li>
      </ul>
      <div class=\"visible-xs breadcrumb-toggle\"><a class=\"btn btn-link btn-lg btn-icon\" data-toggle=\"collapse\" data-target=\".breadcrumb-buttons\"><i class=\"icon-menu2\"></i></a></div>

    </div>
    <!-- /breadcrumbs line -->
    ";
    }

    // line 193
    public function block_nbusers($context, array $blocks = array())
    {
        echo "<h2>22</h2>";
    }

    // line 201
    public function block_nbideas($context, array $blocks = array())
    {
        echo "<h2>22</h2>";
    }

    // line 209
    public function block_nbprojects($context, array $blocks = array())
    {
        echo "<h2>22</h2>";
    }

    // line 217
    public function block_nbclaims($context, array $blocks = array())
    {
        echo "<h2>22</h2>";
    }

    // line 235
    public function block_bundleext_pie($context, array $blocks = array())
    {
    }

    // line 268
    public function block_recentidasactivity($context, array $blocks = array())
    {
        // line 269
        echo "                    
              <li class=\"media\"><a class=\"pull-left\" href=\"#\"><img class=\"media-object\" src=\"";
        // line 270
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/images/demo/users/face2.png"), "html", null, true);
        echo "\" alt=\"\"></a>
                <div class=\"media-body\">
                  <div class=\"clearfix\"><a href=\"#\" class=\"media-heading\">Mahmoud tabka</a><span class=\"media-notice\">January 10, 2016 / 10:20 pm</span></div>

                  Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.</div>
              </li>
                ";
    }

    // line 289
    public function block_recentclaimsactivity($context, array $blocks = array())
    {
        // line 290
        echo "
              <li class=\"media\"><a class=\"pull-left\" href=\"#\"><img class=\"media-object\" src=\"";
        // line 291
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/images/demo/users/face2.png"), "html", null, true);
        echo "\" alt=\"\"></a>
                <div class=\"media-body\">
                  <div class=\"clearfix\"><a href=\"#\" class=\"media-heading\">Mahmoud tabka</a><span class=\"media-notice\">January 10, 2016 / 10:20 pm</span></div>

                  Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.</div>
              </li>
          
             ";
    }

    // line 310
    public function block_recentprojectsactivity($context, array $blocks = array())
    {
        // line 311
        echo "    
              <li class=\"media\"><a class=\"pull-left\" href=\"#\"><img class=\"media-object\" src=\"";
        // line 312
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/images/demo/users/face2.png"), "html", null, true);
        echo "\" alt=\"\"></a>
                <div class=\"media-body\">
                  <div class=\"clearfix\"><a href=\"#\" class=\"media-heading\">Mahmoud tabka</a><span class=\"media-notice\">January 10, 2016 / 10:20 pm</span></div>

                  Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.</div>
              </li>
              
            ";
    }

    public function getTemplateName()
    {
        return "CrowdriseAdministrationBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  667 => 312,  664 => 311,  661 => 310,  649 => 291,  646 => 290,  643 => 289,  632 => 270,  629 => 269,  626 => 268,  621 => 235,  615 => 217,  609 => 209,  603 => 201,  597 => 193,  583 => 176,  580 => 175,  559 => 157,  535 => 320,  533 => 310,  520 => 299,  518 => 289,  504 => 277,  502 => 268,  468 => 236,  466 => 235,  447 => 218,  445 => 217,  436 => 210,  434 => 209,  425 => 202,  423 => 201,  414 => 194,  412 => 193,  404 => 187,  402 => 175,  398 => 173,  396 => 157,  391 => 154,  388 => 153,  383 => 138,  380 => 137,  373 => 134,  370 => 133,  363 => 130,  360 => 129,  353 => 126,  350 => 125,  343 => 122,  340 => 121,  333 => 118,  330 => 117,  310 => 65,  305 => 62,  302 => 61,  292 => 51,  288 => 50,  284 => 49,  280 => 48,  276 => 47,  272 => 46,  268 => 45,  264 => 44,  260 => 43,  256 => 42,  252 => 41,  248 => 40,  244 => 39,  240 => 38,  236 => 37,  232 => 36,  228 => 35,  224 => 34,  220 => 33,  216 => 32,  212 => 31,  208 => 30,  204 => 29,  200 => 28,  196 => 27,  192 => 26,  180 => 17,  176 => 16,  172 => 15,  168 => 14,  157 => 5,  154 => 4,  143 => 341,  141 => 153,  126 => 140,  124 => 137,  121 => 136,  119 => 133,  116 => 132,  114 => 129,  111 => 128,  109 => 125,  106 => 124,  104 => 121,  101 => 120,  99 => 117,  79 => 100,  69 => 93,  53 => 79,  51 => 61,  45 => 57,  43 => 4,  38 => 1,);
    }
}
