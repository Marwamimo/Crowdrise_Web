<?php

/* CrowdriseProjetsBundle:Default:some_projects.html.twig */
class __TwigTemplate_258b9b5adf8a8a29e64cdfd825ea2383fa9adc094f04973f80495f3a2c51ac25 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("CrowdriseProjetsBundle::layout.html.twig");

        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'container' => array($this, 'block_container'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CrowdriseProjetsBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        // line 4
        echo "<head>
<title>Resale a Business Category Flat Bootstrap Responsive Website Template | Categories :: w3layouts</title>


<link rel=\"stylesheet\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap.min.css"), "html", null, true);
        echo "\">
<link rel=\"stylesheet\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap-select.css"), "html", null, true);
        echo "\">
<link href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
<!-- for-mobile-apps -->
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
<meta name=\"keywords\" content=\"Resale Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design\" />
<script type=\"application/x-javascript\"> addEventListener(\"load\", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<!--fonts-->
<link href='//fonts.googleapis.com/css?family=Ubuntu+Condensed' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<!--//fonts-->\t
<!-- js -->


<script type=\"text/javascript\" src=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.min.js"), "html", null, true);
        echo "\"></script>
<!-- js -->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/bootstrap-select.js"), "html", null, true);
        echo "\"></script>

<script>
  \$(document).ready(function () {
    var mySelect = \$('#first-disabled2');

    \$('#special').on('click', function () {
      mySelect.find('option:selected').prop('disabled', true);
      mySelect.selectpicker('refresh');
    });

    \$('#special2').on('click', function () {
      mySelect.find('option:disabled').prop('disabled', false);
      mySelect.selectpicker('refresh');
    });

    \$('#basic2').selectpicker({
      liveSearch: true,
      maxOptions: 1
    });
  });
</script>

<script type=\"text/javascript\" src=\"";
        // line 52
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.leanModal.min.js"), "html", null, true);
        echo "\"></script>
<link href=\"";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/jquery.uls.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"/>
<link href=\"";
        // line 54
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/jquery.uls.grid.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"/>
<link href=\"";
        // line 55
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/jquery.uls.lcd.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"/>
<!-- Source -->
<script src=\"";
        // line 57
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.uls.data.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 58
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.uls.data.utils.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 59
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.uls.lcd.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 60
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.uls.languagefilter.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 61
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.uls.regionfilter.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 62
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.uls.core.js"), "html", null, true);
        echo "\"></script>


<script>
\t\t\t\$( document ).ready( function() {
\t\t\t\t\$( '.uls-trigger' ).uls( {
\t\t\t\t\tonSelect : function( language ) {
\t\t\t\t\t\tvar languageName = \$.uls.data.getAutonym( language );
\t\t\t\t\t\t\$( '.uls-trigger' ).text( languageName );
\t\t\t\t\t},
\t\t\t\t\tquickList: ['en', 'hi', 'he', 'ml', 'ta', 'fr'] //FIXME
\t\t\t\t} );
\t\t\t} );
\t\t</script>
                
<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 77
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/easy-responsive-tabs.css"), "html", null, true);
        echo "\" />
<script src=\"";
        // line 78
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/easyResponsiveTabs.js"), "html", null, true);
        echo "\"></script>
</head>

";
    }

    // line 83
    public function block_container($context, array $blocks = array())
    {
        // line 84
        echo "    
    \t<!-- Categories -->
\t<!--Vertical Tab-->
\t<div class=\"categories-section main-grid-border\">
\t\t<div class=\"container\">
\t\t\t<h2 class=\"head\">Main Categories</h2>
\t\t\t<div class=\"category-list\">
\t\t\t\t<div id=\"parentVerticalTab\">
\t\t\t\t\t<ul class=\"resp-tabs-list hor_1\">
\t\t\t\t\t\t<li>Technology</li>
\t\t\t\t\t\t<li>Health</li>
\t\t\t\t\t\t<li>Arts</li>
\t\t\t\t\t\t<li>Renewable Energy</li>
\t\t\t\t\t\t<li>Furniture</li>
\t\t\t\t\t\t<li>Pets</li>
\t\t\t\t\t\t<li>Books, Sports & Hobbies</li>
\t\t\t\t\t\t<li>Fashion</li>
\t\t\t\t\t\t<li>Kids</li>
\t\t\t\t\t\t<li>Services</li>
\t\t\t\t\t\t<li>Jobs</li>
\t\t\t\t\t\t<li>Real Estate</li>
\t\t\t\t\t\t<a href=\"";
        // line 105
        echo $this->env->getExtension('routing')->getPath("crowdrise_allidees_homepage");
        echo "\">View all Ideas</a>
\t\t\t\t\t</ul>
\t\t\t\t\t<div class=\"resp-tabs-container hor_1\">
\t\t\t\t\t\t<span class=\"active total\" style=\"display:block;\" data-toggle=\"modal\" data-target=\"#myModal\"><strong>All USA</strong> (Select your city to see local ads)</span>
\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t<div class=\"category\">
\t\t\t\t\t\t\t\t<div class=\"category-img\">
\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 112
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/cat1.png"), "html", null, true);
        echo "\" title=\"image\" alt=\"\" />
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"category-info\">
\t\t\t\t\t\t\t\t\t<h4>Mobiles</h4>
\t\t\t\t\t\t\t\t\t<span>5,12,850 Ads</span>
\t\t\t\t\t\t\t\t\t<a href=\"all-classifieds.html\">View Ideas for this category</a>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"sub-categories\">
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li><a href=\"mobiles.html\">mobile phones</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"mobiles.html\">Tablets</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"mobiles.html\">Accessories</a></li>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</div>
                                               
                                                
                                                </div>
\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t<div class=\"category\">
\t\t\t\t\t\t\t\t<div class=\"category-img\">
\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 135
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/cat2.png"), "html", null, true);
        echo "\" title=\"image\" alt=\"\" />
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"category-info\">
\t\t\t\t\t\t\t\t\t<h4>Electronics & Appliances</h4>
\t\t\t\t\t\t\t\t\t<span>2,01,850 Ads</span>
\t\t\t\t\t\t\t\t\t<a href=\"all-classifieds.html\">View all Ads</a>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"sub-categories\">
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li><a href=\"electronics-appliances.html\">Computers & accessories</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"electronics-appliances.html\">Tv - video - audio</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"electronics-appliances.html\">Cameras & accessories</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"electronics-appliances.html\">Games & Entertainment</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"electronics-appliances.html\">Fridge - AC - Washing Machine</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"electronics-appliances.html\">Kitchen & Other Appliances</a></li>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t<div class=\"category\">
\t\t\t\t\t\t\t\t<div class=\"category-img\">
\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 159
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/cat3.png"), "html", null, true);
        echo "\" title=\"image\" alt=\"\" />
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"category-info\">
\t\t\t\t\t\t\t\t\t<h4>Cars</h4>
\t\t\t\t\t\t\t\t\t<span>1,98,080 Ads</span>
\t\t\t\t\t\t\t\t\t<a href=\"all-classifieds.html\">View all Ads</a>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"sub-categories\">
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li><a href=\"cars.html\">Commercial Vehicles</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"cars.html\">Other Vehicles</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"cars.html\">Spare Parts</a></li>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t<div class=\"category\">
\t\t\t\t\t\t\t\t<div class=\"category-img\">
\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 180
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/cat4.png"), "html", null, true);
        echo "\" title=\"image\" alt=\"\" />
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"category-info\">
\t\t\t\t\t\t\t\t\t<h4>Bikes</h4>
\t\t\t\t\t\t\t\t\t<span>6,17,568 Ads</span>
\t\t\t\t\t\t\t\t\t<a href=\"all-classifieds.html\">View all Ads</a>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"sub-categories\">
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li><a href=\"bikes.html\">Motorcycles</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"bikes.html\">Scooters</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"bikes.html\">Bicycles</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"bikes.html\">Spare Parts</a></li>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t<div class=\"category\">
\t\t\t\t\t\t\t\t<div class=\"category-img\">
\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 202
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/cat5.png"), "html", null, true);
        echo "\" title=\"image\" alt=\"\" />
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"category-info\">
\t\t\t\t\t\t\t\t\t<h4>Furniture</h4>
\t\t\t\t\t\t\t\t\t<span>1,05,168 Ads</span>
\t\t\t\t\t\t\t\t\t<a href=\"all-classifieds.html\">View all Ads</a>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"sub-categories\">
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li><a href=\"furnitures.html\">Sofa & Dining</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"furnitures.html\">Beds & Wardrobes</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"furnitures.html\">Home Decor & Garden</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"furnitures.html\">Other Household Items</a></li>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t<div class=\"category\">
\t\t\t\t\t\t\t\t<div class=\"category-img\">
\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 224
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/cat6.png"), "html", null, true);
        echo "\" title=\"image\" alt=\"\" />
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"category-info\">
\t\t\t\t\t\t\t\t\t<h4>Pets</h4>
\t\t\t\t\t\t\t\t\t<span>1,77,816 Ads</span>
\t\t\t\t\t\t\t\t\t<a href=\"all-classifieds.html\">View all Ads</a>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"sub-categories\">
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li><a href=\"pets.html\">Dogs</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"pets.html\">Aquariums</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"pets.html\">Pet Food & Accessories</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"pets.html\">Other Pets</a></li>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
\t<!--Plug-in Initialisation-->
\t<script type=\"text/javascript\">
    \$(document).ready(function() {

        //Vertical Tab
        \$('#parentVerticalTab').easyResponsiveTabs({
            type: 'vertical', //Types: default, vertical, accordion
            width: 'auto', //auto or any width like 600px
            fit: true, // 100% fit in a container
            closed: 'accordion', // Start closed if in accordion view
            tabidentify: 'hor_1', // The tab groups identifier
            activate: function(event) { // Callback function if tab is switched
                var \$tab = \$(this);
                var \$info = \$('#nested-tabInfo2');
                var \$name = \$('span', \$info);
                \$name.text(\$tab.text());
                \$info.show();
            }
        });
    });
</script>
\t<!-- //Categories -->

";
    }

    public function getTemplateName()
    {
        return "CrowdriseProjetsBundle:Default:some_projects.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  330 => 224,  305 => 202,  280 => 180,  256 => 159,  229 => 135,  203 => 112,  193 => 105,  170 => 84,  167 => 83,  159 => 78,  155 => 77,  137 => 62,  133 => 61,  129 => 60,  125 => 59,  121 => 58,  117 => 57,  112 => 55,  108 => 54,  104 => 53,  100 => 52,  74 => 29,  70 => 28,  64 => 25,  46 => 10,  42 => 9,  38 => 8,  32 => 4,  29 => 3,);
    }
}
