<?php

/* CrowdriseIdeesBundle:Default:all_ideas.html.twig */
class __TwigTemplate_f841d93cb1b441d2990e4372e511d7992ade5accae0e2bfd5a2785973680fc51 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("CrowdriseIdeesBundle::layout.html.twig");

        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'container' => array($this, 'block_container'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CrowdriseIdeesBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_head($context, array $blocks = array())
    {
        // line 3
        echo "    
<head>
<title>Resale a Business Category Flat Bootstrap Responsive Website Template | All Classifieds :: w3layouts</title>

<link rel=\"stylesheet\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap.min.css"), "html", null, true);
        echo "\">
<link rel=\"stylesheet\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap-select.css"), "html", null, true);
        echo "\">
<link href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/jquery-ui1.css"), "html", null, true);
        echo "\">

<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
<meta name=\"keywords\" content=\"Resale Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design\" />
<script type=\"application/x-javascript\"> addEventListener(\"load\", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<!--fonts-->
<link href='//fonts.googleapis.com/css?family=Ubuntu+Condensed' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<!--//fonts-->\t
<!-- js -->


<script type=\"text/javascript\" src=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.min.js"), "html", null, true);
        echo "\"></script>
<!-- js -->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/bootstrap-select.js"), "html", null, true);
        echo "\"></script>


<script>
  \$(document).ready(function () {
    var mySelect = \$('#first-disabled2');

    \$('#special').on('click', function () {
      mySelect.find('option:selected').prop('disabled', true);
      mySelect.selectpicker('refresh');
    });

    \$('#special2').on('click', function () {
      mySelect.find('option:disabled').prop('disabled', false);
      mySelect.selectpicker('refresh');
    });

    \$('#basic2').selectpicker({
      liveSearch: true,
      maxOptions: 1
    });
  });
</script>


<script type=\"text/javascript\" src=\"";
        // line 54
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.leanModal.min.js"), "html", null, true);
        echo "\"></script>
<link href=\"";
        // line 55
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/jquery.uls.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"/>
<link href=\"";
        // line 56
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/jquery.uls.grid.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"/>
<link href=\"";
        // line 57
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/jquery.uls.lcd.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"/>
<!-- Source -->
<script src=\"";
        // line 59
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.uls.data.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 60
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.uls.data.utils.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 61
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.uls.lcd.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 62
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.uls.languagefilter.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 63
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.uls.regionfilter.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 64
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.uls.core.js"), "html", null, true);
        echo "\"></script>


<script>
\t\t\t\$( document ).ready( function() {
\t\t\t\t\$( '.uls-trigger' ).uls( {
\t\t\t\t\tonSelect : function( language ) {
\t\t\t\t\t\tvar languageName = \$.uls.data.getAutonym( language );
\t\t\t\t\t\t\$( '.uls-trigger' ).text( languageName );
\t\t\t\t\t},
\t\t\t\t\tquickList: ['en', 'hi', 'he', 'ml', 'ta', 'fr'] //FIXME
\t\t\t\t} );
\t\t\t} );
\t\t</script>
                
                
<script src=\"";
        // line 80
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/tabs.js"), "html", null, true);
        echo "\"></script>
\t
<script type=\"text/javascript\">
\$(document).ready(function () {    
var elem=\$('#container ul');      
\t\$('#viewcontrols a').on('click',function(e) {
\t\tif (\$(this).hasClass('gridview')) {
\t\t\telem.fadeOut(1000, function () {
\t\t\t\t\$('#container ul').removeClass('list').addClass('grid');
\t\t\t\t\$('#viewcontrols').removeClass('view-controls-list').addClass('view-controls-grid');
\t\t\t\t\$('#viewcontrols .gridview').addClass('active');
\t\t\t\t\$('#viewcontrols .listview').removeClass('active');
\t\t\t\telem.fadeIn(1000);
\t\t\t});\t\t\t\t\t\t
\t\t}
\t\telse if(\$(this).hasClass('listview')) {
\t\t\telem.fadeOut(1000, function () {
\t\t\t\t\$('#container ul').removeClass('grid').addClass('list');
\t\t\t\t\$('#viewcontrols').removeClass('view-controls-grid').addClass('view-controls-list');
\t\t\t\t\$('#viewcontrols .gridview').removeClass('active');
\t\t\t\t\$('#viewcontrols .listview').addClass('active');
\t\t\t\telem.fadeIn(1000);
\t\t\t});\t\t\t\t\t\t\t\t\t
\t\t}
\t});
});
</script>
</head>

";
    }

    // line 111
    public function block_container($context, array $blocks = array())
    {
        // line 112
        echo "    <div class=\"total-ads main-grid-border\">
\t\t<div class=\"container\">
\t\t\t<div class=\"select-box\">
\t\t\t\t<div class=\"select-city-for-local-ads ads-list\">
\t\t\t\t\t<label>Select your city to see local ads</label>
\t\t\t\t\t\t<select>
\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"Popular Cities\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<option selected style=\"display:none;color:#eee;\">Entire USA</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Birmingham</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Anchorage</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Phoenix</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Little Rock</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Los Angeles</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Denver</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Bridgeport</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Wilmington</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Jacksonville</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Atlanta</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Honolulu</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Boise</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Chicago</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Indianapolis</option>
\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"More Cities\">
   \t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"Maryland\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Baltimore</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Frederick</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Rockville</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Gaithersburg</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Bowie</option>         \t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"Massachusetts\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Boston</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Worcester</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Springfield</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Lowell</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Cambridge</option>  
\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"Michigan\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Detroit</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Grand Rapids</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Warren</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Sterling Heights</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Lansing</option> 
\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t
\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t            </select>
\t\t\t\t</div>
\t\t\t\t<div class=\"browse-category ads-list\">
\t\t\t\t\t<label>Browse Categories</label>
\t\t\t\t\t<select class=\"selectpicker show-tick\" data-live-search=\"true\">
\t\t\t\t\t  <option data-tokens=\"Mobiles\">All</option>
\t\t\t\t\t  <option data-tokens=\"Mobiles\">Mobiles</option>
\t\t\t\t\t  <option data-tokens=\"Electronics & Appliances\">Electronics & Appliances</option>
\t\t\t\t\t  <option data-tokens=\"Cars\">Cars</option>
\t\t\t\t\t  <option data-tokens=\"Bikes\">Bikes</option>
\t\t\t\t\t  <option data-tokens=\"Furniture\">Furniture</option>
\t\t\t\t\t  <option data-tokens=\"Pets\">Pets</option>
\t\t\t\t\t  <option data-tokens=\"Books, Sports & Hobbies\">Books, Sports & Hobbies</option>
\t\t\t\t\t  <option data-tokens=\"Fashion\">Fashion</option>
\t\t\t\t\t  <option data-tokens=\"Kids\">Kids</option>
\t\t\t\t\t  <option data-tokens=\"Services\">Services</option>
\t\t\t\t\t  <option data-tokens=\"Jobs\">Jobs</option>
\t\t\t\t\t  <option data-tokens=\"Real Estate\">Real Estate</option>
\t\t\t\t\t</select>
\t\t\t\t</div>
\t\t\t\t<div class=\"search-product ads-list\">
\t\t\t\t\t<label>Search for a specific product</label>
\t\t\t\t\t<div class=\"search\">
\t\t\t\t\t\t<div id=\"custom-search-input\">
\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control input-lg\" placeholder=\"Buscar\" />
\t\t\t\t\t\t\t<span class=\"input-group-btn\">
\t\t\t\t\t\t\t\t<button class=\"btn btn-info btn-lg\" type=\"button\">
\t\t\t\t\t\t\t\t\t<i class=\"glyphicon glyphicon-search\"></i>
\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t</div>
\t\t\t<div class=\"all-categories\">
\t\t\t\t<h3> Select your category and find the perfect ad</h3>
\t\t\t\t<ul class=\"all-cat-list\">
\t\t\t\t\t<li><a href=\"mobiles.html\">Mobiles <span class=\"num-of-ads\">(5,78,076)</span></a></li>
\t\t\t\t\t<li><a href=\"electronics-appliances.html\">Electronics & Appliances  <span class=\"num-of-ads\">(3,66,495)</span></a></li>
\t\t\t\t\t<li><a href=\"real-estate.html\">Real Estate  <span class=\"num-of-ads\">(45,450)</span></a></li>
\t\t\t\t\t<li><a href=\"furnitures.html\">Furniture    <span class=\"num-of-ads\">(1,77,145)</span></a></li>
\t\t\t\t\t<li><a href=\"pets.html\">Pets   <span class=\"num-of-ads\">(1,81,250)</span></a></li>
\t\t\t\t\t<li><a href=\"books-sports-hobbies.html\">Books, Sports & Hobbies    <span class=\"num-of-ads\">(66,822)</span></a></li>
\t\t\t\t\t<li><a href=\"fashion.html\">Fashion   <span class=\"num-of-ads\">(29,156)</span></a></li>
\t\t\t\t\t<li><a href=\"kids.html\">Kids   <span class=\"num-of-ads\">(25,699)</span></a></li>
\t\t\t\t\t<li><a href=\"services.html\">Services   <span class=\"num-of-ads\">(2,15,895)</span></a></li>
\t\t\t\t\t<li><a href=\"cars.html\">Cars   <span class=\"num-of-ads\">(2,15,306)</span></a></li>
\t\t\t\t</ul>
\t\t\t</div>
\t\t\t<ol class=\"breadcrumb\" style=\"margin-bottom: 5px;\">
\t\t\t  <li><a href=\"index.html\">Home</a></li>
\t\t\t  <li class=\"active\">All Ads</li>
\t\t\t</ol>
\t\t\t<div class=\"ads-grid\">
\t\t\t\t<div class=\"side-bar col-md-3\">
\t\t\t\t\t<div class=\"search-hotel\">
\t\t\t\t\t<h3 class=\"sear-head\">Name contains</h3>
\t\t\t\t\t<form>
\t\t\t\t\t\t<input type=\"text\" value=\"Product name...\" onfocus=\"this.value = '';\" onblur=\"if (this.value == '') {this.value = 'Product name...';}\" required=\"\">
\t\t\t\t\t\t<input type=\"submit\" value=\" \">
\t\t\t\t\t</form>
\t\t\t\t</div>
\t\t\t\t
\t\t\t\t<div class=\"range\">
\t\t\t\t\t<h3 class=\"sear-head\">Price range</h3>
\t\t\t\t\t\t\t<ul class=\"dropdown-menu6\">
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t                
\t\t\t\t\t\t\t\t\t<div id=\"slider-range\"></div>\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<input type=\"text\" id=\"amount\" style=\"border: 0; color: #ffffff; font-weight: normal;\" />
\t\t\t\t\t\t\t\t\t</li>\t\t\t
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t<!---->
\t\t\t\t\t\t\t<script type=\"text/javascript\" src=\"js/jquery-ui.js\"></script>
\t\t\t\t\t\t\t<script type='text/javascript'>//<![CDATA[ 
\t\t\t\t\t\t\t\$(window).load(function(){
\t\t\t\t\t\t\t \$( \"#slider-range\" ).slider({
\t\t\t\t\t\t\t\t\t\trange: true,
\t\t\t\t\t\t\t\t\t\tmin: 0,
\t\t\t\t\t\t\t\t\t\tmax: 9000,
\t\t\t\t\t\t\t\t\t\tvalues: [ 50, 6000 ],
\t\t\t\t\t\t\t\t\t\tslide: function( event, ui ) {  \$( \"#amount\" ).val( \"\$\" + ui.values[ 0 ] + \" - \$\" + ui.values[ 1 ] );
\t\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t });
\t\t\t\t\t\t\t\$( \"#amount\" ).val( \"\$\" + \$( \"#slider-range\" ).slider( \"values\", 0 ) + \" - \$\" + \$( \"#slider-range\" ).slider( \"values\", 1 ) );

\t\t\t\t\t\t\t});//]]>  

\t\t\t\t\t\t\t</script>
\t\t\t\t\t\t\t
\t\t\t\t</div>
\t\t\t\t<div class=\"featured-ads\">
\t\t\t\t\t<h2 class=\"sear-head fer\">Featured Ads</h2>
\t\t\t\t\t<div class=\"featured-ad\">
\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t<div class=\"featured-ad-left\">
\t\t\t\t\t\t\t\t<img src=\"";
        // line 260
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/f1.jpg"), "html", null, true);
        echo "\" title=\"ad image\" alt=\"\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"featured-ad-right\">
\t\t\t\t\t\t\t\t<h4>Lorem Ipsum is simply dummy text of the printing industry</h4>
\t\t\t\t\t\t\t\t<p>\$ 450</p>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t</a>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"featured-ad\">
\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t<div class=\"featured-ad-left\">
\t\t\t\t\t\t\t\t<img src=\"images/f2.jpg\" title=\"ad image\" alt=\"\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"featured-ad-right\">
\t\t\t\t\t\t\t\t<h4>Lorem Ipsum is simply dummy text of the printing industry</h4>
\t\t\t\t\t\t\t\t<p>\$ 380</p>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t</a>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"featured-ad\">
\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t<div class=\"featured-ad-left\">
\t\t\t\t\t\t\t\t<img src=\"images/f3.jpg\" title=\"ad image\" alt=\"\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"featured-ad-right\">
\t\t\t\t\t\t\t\t<h4>Lorem Ipsum is simply dummy text of the printing industry</h4>
\t\t\t\t\t\t\t\t<p>\$ 560</p>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t</a>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"ads-display col-md-9\">
\t\t\t\t\t<div class=\"wrapper\">\t\t\t\t\t
\t\t\t\t\t<div class=\"bs-example bs-example-tabs\" role=\"tabpanel\" data-example-id=\"togglable-tabs\">
\t\t\t\t\t  <ul id=\"myTab\" class=\"nav nav-tabs nav-tabs-responsive\" role=\"tablist\">
\t\t\t\t\t\t<li role=\"presentation\" class=\"active\">
\t\t\t\t\t\t  <a href=\"#home\" id=\"home-tab\" role=\"tab\" data-toggle=\"tab\" aria-controls=\"home\" aria-expanded=\"true\">
\t\t\t\t\t\t\t<span class=\"text\">All Ads</span>
\t\t\t\t\t\t  </a>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li role=\"presentation\" class=\"next\">
\t\t\t\t\t\t  <a href=\"#profile\" role=\"tab\" id=\"profile-tab\" data-toggle=\"tab\" aria-controls=\"profile\">
\t\t\t\t\t\t\t<span class=\"text\">Ads with Photos</span>
\t\t\t\t\t\t  </a>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li role=\"presentation\">
\t\t\t\t\t\t  <a href=\"#samsa\" role=\"tab\" id=\"samsa-tab\" data-toggle=\"tab\" aria-controls=\"samsa\">
\t\t\t\t\t\t\t<span class=\"text\">Company</span>
\t\t\t\t\t\t  </a>
\t\t\t\t\t\t</li>
\t\t\t\t\t  </ul>
\t\t\t\t\t  <div id=\"myTabContent\" class=\"tab-content\">
\t\t\t\t\t\t<div role=\"tabpanel\" class=\"tab-pane fade in active\" id=\"home\" aria-labelledby=\"home-tab\">
\t\t\t\t\t\t   <div>
\t\t\t\t\t\t\t\t\t\t\t\t<div id=\"container\">
\t\t\t\t\t\t\t\t<div class=\"view-controls-list\" id=\"viewcontrols\">
\t\t\t\t\t\t\t\t\t<label>view :</label>
\t\t\t\t\t\t\t\t\t<a class=\"gridview\"><i class=\"glyphicon glyphicon-th\"></i></a>
\t\t\t\t\t\t\t\t\t<a class=\"listview active\"><i class=\"glyphicon glyphicon-th-list\"></i></a>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"sort\">
\t\t\t\t\t\t\t\t   <div class=\"sort-by\">
\t\t\t\t\t\t\t\t\t\t<label>Sort By : </label>
\t\t\t\t\t\t\t\t\t\t<select>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"\">Most recent</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"\">Price: Rs Low to High</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"\">Price: Rs High to Low</option>
\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t   </div>
\t\t\t\t\t\t\t\t\t </div>
\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t<ul class=\"list\">
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 339
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/m1.jpg"), "html", null, true);
        echo "\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">There are many variations of passages of Lorem Ipsum</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$290</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:55</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m2.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">It is a long established fact that a reader long established</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$310</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:45</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m3.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">Contrary to popular belief, Lorem Ipsum is not</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$190</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:30</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m4.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">The standard chunk of Lorem Ipsum used since the</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$480</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:25</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m5.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">Sed ut perspiciatis unde omnis iste natus error sit voluptatem</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$859</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:24</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m6.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">But I must explain to you how all this mistaken idea of denouncing</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$1299</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:22</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m1.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$1099</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:21</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m7.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">On the other hand, we denounce with righteous dislike men</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$290</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:20</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m8.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">There are many variations of passages of Lorem Ipsum</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$899</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:05</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m9.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">ducimus qui blanditiis praesentium voluptatum quos dolores et qua</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$199</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:04</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m10.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">There are many variations of passages of Lorem Ipsum</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$250</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:03</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m11.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">who are so beguiled and demoralized by the charms of pleasure of the moment</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$189</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:03</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m12.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">you need to be sure there isn't anything embarrassing hidden</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$1090</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:03</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m1.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">There are many variations of passages of Lorem Ipsum</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$290</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:55</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m2.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">It is a long established fact that a reader long established</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$310</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:45</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m3.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">Contrary to popular belief, Lorem Ipsum is not</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$190</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:30</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m4.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">The standard chunk of Lorem Ipsum used since the</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$480</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:25</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m5.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">Sed ut perspiciatis unde omnis iste natus error sit voluptatem</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$859</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:24</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m6.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">But I must explain to you how all this mistaken idea of denouncing</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$1299</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:22</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m1.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$1099</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:21</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m7.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">On the other hand, we denounce with righteous dislike men</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$290</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:20</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m8.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">There are many variations of passages of Lorem Ipsum</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$899</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:05</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m9.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">ducimus qui blanditiis praesentium voluptatum quos dolores et qua</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$199</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:04</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m10.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">There are many variations of passages of Lorem Ipsum</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$250</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:03</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m11.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">who are so beguiled and demoralized by the charms of pleasure of the moment</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$189</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:03</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m12.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">you need to be sure there isn't anything embarrassing hidden</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$1090</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:03</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m13.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">looked up one of the more obscure Latin words</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$599</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:02</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div role=\"tabpanel\" class=\"tab-pane fade\" id=\"profile\" aria-labelledby=\"profile-tab\">
\t\t\t\t\t\t <div>
\t\t\t\t\t\t\t\t\t\t\t\t<div id=\"container\">
\t\t\t\t\t\t\t\t<div class=\"view-controls-list\" id=\"viewcontrols\">
\t\t\t\t\t\t\t\t\t<label>view :</label>
\t\t\t\t\t\t\t\t\t<a class=\"gridview\"><i class=\"glyphicon glyphicon-th\"></i></a>
\t\t\t\t\t\t\t\t\t<a class=\"listview active\"><i class=\"glyphicon glyphicon-th-list\"></i></a>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"sort\">
\t\t\t\t\t\t\t\t   <div class=\"sort-by\">
\t\t\t\t\t\t\t\t\t\t<label>Sort By : </label>
\t\t\t\t\t\t\t\t\t\t<select>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"\">Most recent</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"\">Price: Rs Low to High</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"\">Price: Rs High to Low</option>
\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t   </div>
\t\t\t\t\t\t\t\t\t </div>
\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t<ul class=\"list\">
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m1.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">There are many variations of passages of Lorem Ipsum</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$290</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:55</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m2.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">It is a long established fact that a reader long established</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$310</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:45</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m3.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">Contrary to popular belief, Lorem Ipsum is not</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$190</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:30</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m4.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">The standard chunk of Lorem Ipsum used since the</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$480</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:25</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m5.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">Sed ut perspiciatis unde omnis iste natus error sit voluptatem</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$859</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:24</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m6.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">But I must explain to you how all this mistaken idea of denouncing</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$1299</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:22</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m1.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$1099</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:21</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m7.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">On the other hand, we denounce with righteous dislike men</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$290</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:20</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m8.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">There are many variations of passages of Lorem Ipsum</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$899</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:05</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m9.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">ducimus qui blanditiis praesentium voluptatum quos dolores et qua</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$199</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:04</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m10.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">There are many variations of passages of Lorem Ipsum</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$250</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:03</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m11.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">who are so beguiled and demoralized by the charms of pleasure of the moment</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$189</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:03</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m12.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">you need to be sure there isn't anything embarrassing hidden</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$1090</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:03</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m13.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">looked up one of the more obscure Latin words</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$599</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:02</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div role=\"tabpanel\" class=\"tab-pane fade\" id=\"samsa\" aria-labelledby=\"samsa-tab\">
\t\t\t\t\t\t  <div>
\t\t\t\t\t\t\t\t\t\t\t\t<div id=\"container\">
\t\t\t\t\t\t\t\t<div class=\"view-controls-list\" id=\"viewcontrols\">
\t\t\t\t\t\t\t\t\t<label>view :</label>
\t\t\t\t\t\t\t\t\t<a class=\"gridview\"><i class=\"glyphicon glyphicon-th\"></i></a>
\t\t\t\t\t\t\t\t\t<a class=\"listview active\"><i class=\"glyphicon glyphicon-th-list\"></i></a>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"sort\">
\t\t\t\t\t\t\t\t   <div class=\"sort-by\">
\t\t\t\t\t\t\t\t\t\t<label>Sort By : </label>
\t\t\t\t\t\t\t\t\t\t<select>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"\">Most recent</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"\">Price: Rs Low to High</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"\">Price: Rs High to Low</option>
\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t   </div>
\t\t\t\t\t\t\t\t\t </div>
\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t<ul class=\"list\">
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m1.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">There are many variations of passages of Lorem Ipsum</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$290</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:55</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m2.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">It is a long established fact that a reader long established</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$310</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:45</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m3.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">Contrary to popular belief, Lorem Ipsum is not</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$190</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:30</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m4.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">The standard chunk of Lorem Ipsum used since the</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$480</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:25</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m5.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">Sed ut perspiciatis unde omnis iste natus error sit voluptatem</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$859</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:24</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m6.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">But I must explain to you how all this mistaken idea of denouncing</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$1299</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:22</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m12.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">you need to be sure there isn't anything embarrassing hidden</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$1090</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:03</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m1.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$1099</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:21</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m7.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">On the other hand, we denounce with righteous dislike men</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$290</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:20</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m8.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">There are many variations of passages of Lorem Ipsum</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$899</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:05</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m13.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">looked up one of the more obscure Latin words</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$599</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:02</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m9.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">ducimus qui blanditiis praesentium voluptatum quos dolores et qua</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$199</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:04</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m10.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">There are many variations of passages of Lorem Ipsum</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$250</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:03</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m12.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">you need to be sure there isn't anything embarrassing hidden</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$1090</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:03</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m11.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">who are so beguiled and demoralized by the charms of pleasure of the moment</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$189</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:03</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m4.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">The standard chunk of Lorem Ipsum used since the</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$480</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:25</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m9.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">ducimus qui blanditiis praesentium voluptatum quos dolores et qua</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$199</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:04</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m8.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">There are many variations of passages of Lorem Ipsum</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$899</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:05</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m12.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">you need to be sure there isn't anything embarrassing hidden</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$1090</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:03</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m11.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">who are so beguiled and demoralized by the charms of pleasure of the moment</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$189</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:03</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<img src=\"images/m13.jpg\" title=\"\" alt=\"\" />
\t\t\t\t\t\t\t\t\t<section class=\"list-left\">
\t\t\t\t\t\t\t\t\t<h5 class=\"title\">looked up one of the more obscure Latin words</h5>
\t\t\t\t\t\t\t\t\t<span class=\"adprice\">\$599</span>
\t\t\t\t\t\t\t\t\t<p class=\"catpath\">Mobile Phones » Brand</p>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<section class=\"list-right\">
\t\t\t\t\t\t\t\t\t<span class=\"date\">Today, 17:02</span>
\t\t\t\t\t\t\t\t\t<span class=\"cityname\">City name</span>
\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t\t</li> 
\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<ul class=\"pagination pagination-sm\">
\t\t\t\t\t\t\t<li><a href=\"#\">Prev</a></li>
\t\t\t\t\t\t\t<li><a href=\"#\">1</a></li>
\t\t\t\t\t\t\t<li><a href=\"#\">2</a></li>
\t\t\t\t\t\t\t<li><a href=\"#\">3</a></li>
\t\t\t\t\t\t\t<li><a href=\"#\">4</a></li>
\t\t\t\t\t\t\t<li><a href=\"#\">5</a></li>
\t\t\t\t\t\t\t<li><a href=\"#\">6</a></li>
\t\t\t\t\t\t\t<li><a href=\"#\">7</a></li>
\t\t\t\t\t\t\t<li><a href=\"#\">8</a></li>
\t\t\t\t\t\t\t<li><a href=\"#\">Next</a></li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t  </div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t</div>
\t\t</div>\t
\t</div>
\t<!-- // Products -->
";
    }

    public function getTemplateName()
    {
        return "CrowdriseIdeesBundle:Default:all_ideas.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  431 => 339,  349 => 260,  199 => 112,  196 => 111,  162 => 80,  143 => 64,  139 => 63,  135 => 62,  131 => 61,  127 => 60,  123 => 59,  118 => 57,  114 => 56,  110 => 55,  106 => 54,  78 => 29,  74 => 28,  68 => 25,  50 => 10,  46 => 9,  42 => 8,  38 => 7,  32 => 3,  29 => 2,);
    }
}
