<?php

/* CrowdriseMembreBundle:Default:login.html.twig */
class __TwigTemplate_29cd2911dd902ebfddb0bca5da5280ccff45d1136263b9398936157aa88a89eb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("CrowdriseMembreBundle::layout.html.twig");

        $this->blocks = array(
            'container' => array($this, 'block_container'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CrowdriseMembreBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_container($context, array $blocks = array())
    {
        // line 5
        echo "     <section>
\t\t\t<div id=\"page-wrapper\" class=\"sign-in-wrapper\">
\t\t\t\t<div class=\"graphs\">
\t\t\t\t\t<div class=\"sign-in-form\">
\t\t\t\t\t\t<div class=\"sign-in-form-top\">
\t\t\t\t\t\t\t<h1>Log in</h1>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"signin\">
\t\t\t\t\t\t\t<div class=\"signin-rit\">
\t\t\t\t\t\t\t\t<span class=\"checkbox1\">
\t\t\t\t\t\t\t\t\t <label class=\"checkbox\"><input type=\"checkbox\" name=\"checkbox\" checked=\"\">Forgot Password ?</label>
\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t<p><a href=\"#\">Click Here</a> </p>
\t\t\t\t\t\t\t\t<div class=\"clearfix\"> </div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<form>
\t\t\t\t\t\t\t<div class=\"log-input\">
\t\t\t\t\t\t\t\t<div class=\"log-input-left\">
\t\t\t\t\t\t\t\t   <input type=\"text\" class=\"user\" value=\"Your Email\" onfocus=\"this.value = '';\" onblur=\"if (this.value == '') {this.value = 'Your Email';}\"/>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<span class=\"checkbox2\">
\t\t\t\t\t\t\t\t\t <label class=\"checkbox\"><input type=\"checkbox\" name=\"checkbox\" checked=\"\"><i> </i></label>
\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t<div class=\"clearfix\"> </div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"log-input\">
\t\t\t\t\t\t\t\t<div class=\"log-input-left\">
\t\t\t\t\t\t\t\t   <input type=\"password\" class=\"lock\" value=\"password\" onfocus=\"this.value = '';\" onblur=\"if (this.value == '') {this.value = 'Email address:';}\"/>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<span class=\"checkbox2\">
\t\t\t\t\t\t\t\t\t <label class=\"checkbox\"><input type=\"checkbox\" name=\"checkbox\" checked=\"\"><i> </i></label>
\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t<div class=\"clearfix\"> </div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<input type=\"submit\" value=\"Log in\">
\t\t\t\t\t\t</form>\t 
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"new_people\">
\t\t\t\t\t\t\t<h2>For New People</h2>
\t\t\t\t\t\t\t<p>Having hands on experience in creating innovative designs,I do offer design 
\t\t\t\t\t\t\t\tsolutions which harness.</p>
\t\t\t\t\t\t\t<a href=\"";
        // line 46
        echo $this->env->getExtension('routing')->getPath("crowdrise_membre_registerpage");
        echo "\">Register Now!</a>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>

\t</section>
";
    }

    public function getTemplateName()
    {
        return "CrowdriseMembreBundle:Default:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  74 => 46,  31 => 5,  28 => 4,);
    }
}
