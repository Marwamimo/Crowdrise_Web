<?php

use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Psr\Log\LoggerInterface;

/**
 * appProdUrlGenerator
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdUrlGenerator extends Symfony\Component\Routing\Generator\UrlGenerator
{
    private static $declaredRoutes = array(
        'crowdrise_membre_homepage' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'Crowdrise\\MembreBundle\\Controller\\DefaultController::indexAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/accueil',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'crowdrise_membre_loginpage' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'Crowdrise\\MembreBundle\\Controller\\DefaultController::loginAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/login',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'crowdrise_membre_registerpage' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'Crowdrise\\MembreBundle\\Controller\\DefaultController::registerAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/register',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'crowdrise_problemes_homepage' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'Crowdrise\\ProblemesBundle\\Controller\\DefaultController::problemsAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/stackprobs',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'crowdrise_profile_homepage' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'Crowdrise\\ProfileBundle\\Controller\\DefaultController::profileAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/profile',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'crowdrise_idees_homepage' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'Crowdrise\\IdeesBundle\\Controller\\DefaultController::someideasAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/ideas',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'crowdrise_allidees_homepage' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'Crowdrise\\IdeesBundle\\Controller\\DefaultController::allideasAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/allIdeas',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'crowdrise_projets_homepage' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'Crowdrise\\ProjetsBundle\\Controller\\DefaultController::projectsAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/projects',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'crowdrise_allprojets_homepage' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'Crowdrise\\ProjetsBundle\\Controller\\DefaultController::allprojectsAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/allProjects',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'crowdrise_administration_homepage' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'Crowdrise\\AdministrationBundle\\Controller\\DefaultController::administrationAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/administration_home',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'crowdrise_administration_users' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'Crowdrise\\AdministrationBundle\\Controller\\UtilisateurController::affichageUtilisateursAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/administration_users',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'crowdrise_administration_delete_user' => array (  0 =>   array (    0 => 'id',  ),  1 =>   array (    '_controller' => 'Crowdrise\\AdministrationBundle\\Controller\\UtilisateurController::suppUtilisateurAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'id',    ),    1 =>     array (      0 => 'text',      1 => '/deleteuser',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'crowdrise_administration_ideas_requests' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'Crowdrise\\AdministrationBundle\\Controller\\IdeeController::affichageIdeesAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/administration_ideasrequests',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'crowdrise_administration_accept_idea' => array (  0 =>   array (    0 => 'id',  ),  1 =>   array (    '_controller' => 'Crowdrise\\AdministrationBundle\\Controller\\IdeeController::acceptIdeeAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'id',    ),    1 =>     array (      0 => 'text',      1 => '/acceptidea',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'crowdrise_administration_refuse_idea' => array (  0 =>   array (    0 => 'id',  ),  1 =>   array (    '_controller' => 'Crowdrise\\AdministrationBundle\\Controller\\IdeeController::refuseIdeaAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'id',    ),    1 =>     array (      0 => 'text',      1 => '/refuseidea',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'crowdrise_administration_claims' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'Crowdrise\\AdministrationBundle\\Controller\\ReclamationController::affichageReclamationsAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/administration_claims',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'crowdrise_administration_delete_claim' => array (  0 =>   array (    0 => 'id',  ),  1 =>   array (    '_controller' => 'Crowdrise\\AdministrationBundle\\Controller\\ReclamationController::suppReclamationAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'id',    ),    1 =>     array (      0 => 'text',      1 => '/deleteclaim',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'crowdrise_administration_projects' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'Crowdrise\\AdministrationBundle\\Controller\\ProjetController::affichageProjetsAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/administration_projects',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'crowdrise_administration_delete_project' => array (  0 =>   array (    0 => 'id',  ),  1 =>   array (    '_controller' => 'Crowdrise\\AdministrationBundle\\Controller\\ProjetController::suppProjetAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'id',    ),    1 =>     array (      0 => 'text',      1 => '/deleteproject',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
    );

    /**
     * Constructor.
     */
    public function __construct(RequestContext $context, LoggerInterface $logger = null)
    {
        $this->context = $context;
        $this->logger = $logger;
    }

    public function generate($name, $parameters = array(), $referenceType = self::ABSOLUTE_PATH)
    {
        if (!isset(self::$declaredRoutes[$name])) {
            throw new RouteNotFoundException(sprintf('Unable to generate a URL for the named route "%s" as such route does not exist.', $name));
        }

        list($variables, $defaults, $requirements, $tokens, $hostTokens, $requiredSchemes) = self::$declaredRoutes[$name];

        return $this->doGenerate($variables, $defaults, $requirements, $tokens, $parameters, $name, $referenceType, $hostTokens, $requiredSchemes);
    }
}
