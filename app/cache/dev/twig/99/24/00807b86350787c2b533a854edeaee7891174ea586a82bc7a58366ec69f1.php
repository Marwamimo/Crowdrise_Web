<?php

/* CrowdriseAdministrationBundle:Default:users.html.twig */
class __TwigTemplate_992400807b86350787c2b533a854edeaee7891174ea586a82bc7a58366ec69f1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("CrowdriseAdministrationBundle::layout.html.twig");

        $this->blocks = array(
            'nav_li_dash' => array($this, 'block_nav_li_dash'),
            'nav_li_users' => array($this, 'block_nav_li_users'),
            'pagecontent' => array($this, 'block_pagecontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CrowdriseAdministrationBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_nav_li_dash($context, array $blocks = array())
    {
        // line 4
        echo "<li><a href=\"";
        echo $this->env->getExtension('routing')->getPath("crowdrise_administration_homepage");
        echo "\"><span>Dashboard</span> <i class=\"icon-screen2\"></i></a></li>
";
    }

    // line 7
    public function block_nav_li_users($context, array $blocks = array())
    {
        // line 8
        echo "<li class=\"active\"><a href=\"";
        echo $this->env->getExtension('routing')->getPath("crowdrise_administration_users");
        echo "\"><span>Users</span> <i class=\"icon-users2\"></i></a></li>
";
    }

    // line 12
    public function block_pagecontent($context, array $blocks = array())
    {
        // line 13
        echo "    
    
  <!-- Page content -->


  <div class=\"page-content\">
    <!-- Page header -->
    <div class=\"page-header\">
      <div class=\"page-title\">
        <h3>Users <small>Users managment</small></h3>
      </div>
      <div id=\"reportrange\" class=\"range\">
        <div class=\"visible-xs header-element-toggle\"><a class=\"btn btn-primary btn-icon\"><i class=\"icon-calendar\"></i></a></div>
        <div class=\"date-range\"></div>
        <span class=\"label label-danger\">9</span></div>
    </div>
    <!-- /page header -->

    <!-- Breadcrumbs line -->
    <div class=\"breadcrumb-line\">
      <ul class=\"breadcrumb\">
        <li><a href=\"index.html\">Home</a></li>
        <li class=\"active\">Users</li>
      </ul>

   
    </div>
    <!-- /breadcrumbs line -->



    <!-- Page tabs -->
    <div class=\"tabbable page-tabs\">

      <div class=\"tab-content\">

        <!-- Third tab -->
        <div class=\"active\" id=\"list-view\">
          <!-- Table view -->
          <div class=\"panel panel-default\">
            <div class=\"panel-heading\">
              <h5 class=\"panel-title\"><i class=\"icon-users2\"></i> Users List</h5>
              <span class=\"label label-danger pull-right\">+23</span> </div>
            <div class=\"datatable-media\">
              <table class=\"table table-bordered table-striped\">


                <thead>

                  <tr>
                    <th class=\"image-column\">Image</th>
                    <th>First & Last Name</th>
                    <th>Username</th>
                    <th>Profile info</th>
                    <th class=\"team-links\">Actions</th>
                  </tr>

                </thead>


                <tbody>
                    
                    
                 
                ";
        // line 77
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["utilisateurs"]) ? $context["utilisateurs"] : $this->getContext($context, "utilisateurs")));
        foreach ($context['_seq'] as $context["_key"] => $context["u"]) {
            // line 78
            echo "
                  <tr>                    
                        
                    <td class=\"text-center\"><a href=\"images/demo/users/face1.png\" class=\"lightbox\" title=\"Eugene A. Kopyov\"><img src=\"";
            // line 81
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/images/demo/users/avatar.png"), "html", null, true);
            echo "\" alt=\"\" class=\"img-media\"></a></td>


                    <td class=\"text-semibold\">";
            // line 84
            echo twig_escape_filter($this->env, $this->getAttribute($context["u"], "prenomUtilisateur", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["u"], "nomUtilisateur", array()), "html", null, true);
            echo "</td>

                    <td class=\"muted\">";
            // line 86
            echo twig_escape_filter($this->env, $this->getAttribute($context["u"], "username", array()), "html", null, true);
            echo "</td>


                    <td class=\"file-info\">
                       <span><strong>Email :</strong> ";
            // line 90
            echo twig_escape_filter($this->env, $this->getAttribute($context["u"], "email", array()), "html", null, true);
            echo "</span>
                       <span><strong>Chips :</strong> <a href=\"#\"><b>";
            // line 91
            echo twig_escape_filter($this->env, $this->getAttribute($context["u"], "note", array()), "html", null, true);
            echo "</b></a></span>
                       <span><strong>No. of projects :</strong> <a href=\"#\"><b>";
            // line 92
            echo twig_escape_filter($this->env, $this->getAttribute($context["u"], "nbProjets", array()), "html", null, true);
            echo "</b></a></span>
                       <span><strong>No. of ideas :</strong> <a href=\"#\"><b>";
            // line 93
            echo twig_escape_filter($this->env, $this->getAttribute($context["u"], "nbIdees", array()), "html", null, true);
            echo "</b></a></span>

                    </td>

                    <td class=\"text-center\"><div class=\"btn-group\">
                        <button type=\"button\" class=\"btn btn-icon btn-success dropdown-toggle\" data-toggle=\"dropdown\"><i class=\"icon-cog4\"></i></button>
                        <ul class=\"dropdown-menu icons-right dropdown-menu-right\">
                          <li><a href=\"";
            // line 100
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("crowdrise_administration_delete_user", array("id" => $this->getAttribute($context["u"], "idUtilisateur", array()))), "html", null, true);
            echo "\"><i class=\"icon-remove4\"></i> Delete</a></li>
                          <li><a href=\"";
            // line 101
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("crowdrise_administration_block_user", array("id" => $this->getAttribute($context["u"], "idUtilisateur", array()))), "html", null, true);
            echo "\"><i class=\"icon-blocked\"></i> Block</a></li>
       
           
                        </ul>
                      </div></td>


                  </tr>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['u'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 110
        echo "


                </tbody>


              </table>
            </div>
          </div>
          <!-- /table view -->
        </div>
        <!-- /third tab -->
      </div>
    </div>
    <!-- page tabs -->


    <!-- Footer -->
    <div class=\"footer clearfix\">
      <div class=\"pull-left\">&copy; 2016. Crowdrise Admin by <a href=\"http://themeforest.net/user/Kopyov\">DevX Team</a></div>
      <div class=\"pull-right icons-group\"> <a href=\"#\"><i class=\"icon-screen2\"></i></a> <a href=\"#\"><i class=\"icon-balance\"></i></a> <a href=\"#\"><i class=\"icon-cog3\"></i></a> </div>
    </div>
    <!-- /footer -->
  </div>
  <!-- /page content -->
    
";
    }

    public function getTemplateName()
    {
        return "CrowdriseAdministrationBundle:Default:users.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  189 => 110,  174 => 101,  170 => 100,  160 => 93,  156 => 92,  152 => 91,  148 => 90,  141 => 86,  134 => 84,  128 => 81,  123 => 78,  119 => 77,  53 => 13,  50 => 12,  43 => 8,  40 => 7,  33 => 4,  30 => 3,);
    }
}
