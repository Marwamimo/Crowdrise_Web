<?php

/* CrowdriseIdeesBundle::index.html.twig */
class __TwigTemplate_7c48289a5b5e1f228b68bde7f275ed5566d7f7f2f4a4cb89f2b5991269513ea3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
";
        // line 3
        $this->displayBlock('head', $context, $blocks);
        // line 73
        echo "
";
        // line 74
        $this->displayBlock('body', $context, $blocks);
        // line 885
        echo "</html>";
    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        // line 4
        echo "<head>
<title>Resale a Business Category Flat Bootstrap Responsive Website Template | Home :: w3layouts</title>
<link rel=\"stylesheet\" href=\"css/bootstrap.min.css\">
<link rel=\"stylesheet\" href=\"css/bootstrap-select.css\">
<link href=\"css/style.css\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
<link rel=\"stylesheet\" href=\"css/flexslider.css\" type=\"text/css\" media=\"screen\" />
<link rel=\"stylesheet\" href=\"css/font-awesome.min.css\" />
<!-- for-mobile-apps -->
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
<meta name=\"keywords\" content=\"Resale Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design\" />
<script type=\"application/x-javascript\"> addEventListener(\"load\", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<!--fonts-->
<link href='//fonts.googleapis.com/css?family=Ubuntu+Condensed' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<!--//fonts-->\t
<!-- js -->
<script type=\"text/javascript\" src=\"js/jquery.min.js\"></script>
<!-- js -->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src=\"js/bootstrap.min.js\"></script>
<script src=\"js/bootstrap-select.js\"></script>
<script>
  \$(document).ready(function () {
    var mySelect = \$('#first-disabled2');

    \$('#special').on('click', function () {
      mySelect.find('option:selected').prop('disabled', true);
      mySelect.selectpicker('refresh');
    });

    \$('#special2').on('click', function () {
      mySelect.find('option:disabled').prop('disabled', false);
      mySelect.selectpicker('refresh');
    });

    \$('#basic2').selectpicker({
      liveSearch: true,
      maxOptions: 1
    });
  });
</script>
<script type=\"text/javascript\" src=\"js/jquery.leanModal.min.js\"></script>
<link href=\"css/jquery.uls.css\" rel=\"stylesheet\"/>
<link href=\"css/jquery.uls.grid.css\" rel=\"stylesheet\"/>
<link href=\"css/jquery.uls.lcd.css\" rel=\"stylesheet\"/>
<!-- Source -->
<script src=\"js/jquery.uls.data.js\"></script>
<script src=\"js/jquery.uls.data.utils.js\"></script>
<script src=\"js/jquery.uls.lcd.js\"></script>
<script src=\"js/jquery.uls.languagefilter.js\"></script>
<script src=\"js/jquery.uls.regionfilter.js\"></script>
<script src=\"js/jquery.uls.core.js\"></script>
<script>
\t\t\t\$( document ).ready( function() {
\t\t\t\t\$( '.uls-trigger' ).uls( {
\t\t\t\t\tonSelect : function( language ) {
\t\t\t\t\t\tvar languageName = \$.uls.data.getAutonym( language );
\t\t\t\t\t\t\$( '.uls-trigger' ).text( languageName );
\t\t\t\t\t},
\t\t\t\t\tquickList: ['en', 'hi', 'he', 'ml', 'ta', 'fr'] //FIXME
\t\t\t\t} );
\t\t\t} );
\t\t</script>

</head>
";
    }

    // line 74
    public function block_body($context, array $blocks = array())
    {
        // line 75
        echo "<body>
\t<div class=\"header\">
\t\t<div class=\"container\">
\t\t\t<div class=\"logo\">
\t\t\t\t<a href=\"index.html\"><span>Re</span>sale</a>
\t\t\t</div>
\t\t\t<div class=\"header-right\">
\t\t\t<a class=\"account\" href=\"login.html\">My Account</a>
\t\t\t<span class=\"active uls-trigger\">Select language</span>
\t<!-- Large modal -->
\t\t\t<div class=\"selectregion\">
\t\t\t\t<button class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#myModal\">
\t\t\t\tSelect Your Region</button>
\t\t\t\t\t<div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\"
\t\t\t\t\taria-hidden=\"true\">
\t\t\t\t\t\t<div class=\"modal-dialog modal-lg\">
\t\t\t\t\t\t\t<div class=\"modal-content\">
\t\t\t\t\t\t\t\t<div class=\"modal-header\">
\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">
\t\t\t\t\t\t\t\t\t\t&times;</button>
\t\t\t\t\t\t\t\t\t<h4 class=\"modal-title\" id=\"myModalLabel\">
\t\t\t\t\t\t\t\t\t\tPlease Choose Your Location</h4>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"modal-body\">
\t\t\t\t\t\t\t\t\t <form class=\"form-horizontal\" role=\"form\">
\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t\t\t<select id=\"basic2\" class=\"show-tick form-control\" multiple>
\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"Popular Cities\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<option selected style=\"display:none;color:#eee;\">Select City</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Birmingham</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Anchorage</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Phoenix</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Little Rock</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Los Angeles</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Denver</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Bridgeport</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Wilmington</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Jacksonville</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Atlanta</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Honolulu</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Boise</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Chicago</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Indianapolis</option>
\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"More Cities\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"Alabama\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Birmingham</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Montgomery</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Mobile</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Huntsville</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Tuscaloosa</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"Alaska\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Anchorage</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Fairbanks</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Juneau</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Sitka</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Ketchikan</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"Arizona\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Phoenix</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Tucson</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Mesa</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Chandler</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Glendale</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"Arkansas\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Little Rock</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Fort Smith</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Fayetteville</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Springdale</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Jonesboro</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"California\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Los Angeles</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>San Diego</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>San Jose</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>San Francisco</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Fresno</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"Colorado\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Denver</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Colorado</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Aurora</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Fort Collins</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Lakewood</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"Connecticut\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Bridgeport</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>New Haven</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Hartford</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Stamford</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Waterbury</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"Delaware\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Wilmington</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Dover</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Newark</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Bear</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Middletown</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"Florida\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Jacksonville</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Miami</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Tampa</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>St. Petersburg</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Orlando</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"Georgia\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Atlanta</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Augusta</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Columbus</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Savannah</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Athens</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"Hawaii\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Honolulu</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Pearl City</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Hilo</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Kailua</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Waipahu</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"Idaho\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Boise</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Nampa</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Meridian</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Idaho Falls</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Pocatello</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"Illinois\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Chicago</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Aurora</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Rockford</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Joliet</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Naperville</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"Indiana\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Indianapolis</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Fort Wayne</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Evansville</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>South Bend</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Hammond</option>\t\t\t\t\t\t\t\t\t\t\t\t\t\t       
\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"Iowa\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Des Moines</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Cedar Rapids</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Davenport</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Sioux City</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Waterloo</option>       \t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"Kansas\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Wichita</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Overland Park</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Kansas City</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Topeka</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Olathe  </option>            \t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"Kentucky\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Louisville</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Lexington</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Bowling Green</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Owensboro</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Covington</option>        \t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"Louisiana\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>New Orleans</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Baton Rouge</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Shreveport</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Metairie</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Lafayette</option>          \t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"Maine\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Portland</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Lewiston</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Bangor</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>South Portland</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Auburn</option>         \t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"Maryland\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Baltimore</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Frederick</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Rockville</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Gaithersburg</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Bowie</option>         \t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"Massachusetts\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Boston</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Worcester</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Springfield</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Lowell</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Cambridge</option>  
\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"Michigan\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Detroit</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Grand Rapids</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Warren</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Sterling Heights</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Lansing</option> 
\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"Minnesota\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Minneapolis</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>St. Paul</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Rochester</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Duluth</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Bloomington</option>      \t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"Mississippi\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Jackson</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Gulfport</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Southaven</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Hattiesburg</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Biloxi</option>         \t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"Missouri\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Kansas City</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>St. Louis</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Springfield</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Independence</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Columbia</option>            \t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"Montana\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Billings</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Missoula</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Great Falls</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Bozeman</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Butte-Silver Bow</option>         \t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"Nebraska\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Omaha</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Lincoln</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Bellevue</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Grand Island</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Kearney</option>        \t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"Nevada\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Las Vegas</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Henderson</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>North Las Vegas</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Reno</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Sunrise Manor</option>            \t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"New Hampshire\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Manchesters</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Nashua</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Concord</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Dover</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Rochester</option>              \t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"New Jersey\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Newark</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Jersey City</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Paterson</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Elizabeth</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Edison</option> 
\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"New Mexico\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Albuquerque</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Las Cruces</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Rio Rancho</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Santa Fe</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Roswell</option>       
\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"New York\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>New York</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Buffalo</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Rochester</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Yonkers</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Syracuse</option>        \t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"North Carolina\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Charlotte</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Raleigh</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Greensboro</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Winston-Salem</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Durham</option>          \t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"North Dakota\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Fargo</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Bismarck</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Grand Forks</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Minot</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>West Fargo</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"Ohio\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Columbus</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Cleveland</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Cincinnati</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Toledo</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Akron</option>      
\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"Oklahoma\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Oklahoma City</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Tulsa</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Norman</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Broken Arrow</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Lawton</option>        \t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"Oregon\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Portland</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Eugene</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Salem</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Gresham</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Hillsboro</option>          \t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"Pennsylvania\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Philadelphia</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Pittsburgh</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Allentown</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Erie</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Reading</option>         \t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"Rhode Island\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Providence</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Warwick</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Cranston</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Pawtucket</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>East Providence</option>   
\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"South Carolina\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Columbia</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Charleston</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>North Charleston</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Mount Pleasant</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Rock Hill</option> 
\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"South Dakota\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Sioux Falls</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Rapid City</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Aberdeen</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Brookings</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Watertown</option> 
\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"Tennessee\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Memphis</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Nashville</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Knoxville</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Chattanooga</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Clarksville</option>       
\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"Texas\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Houston</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>San Antonio</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Dallas</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Austin</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Fort Worth</option>   
\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"Utah\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Salt Lake City</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>West Valley City</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Provo</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>West Jordan</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Orem</option>   
\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>\t
\t\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"Vermont\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Burlington</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Essex</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>South Burlington</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Colchester</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Rutland</option>   
\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"Virginia\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Virginia Beach</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Norfolk</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Chesapeake</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Arlington</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Richmond</option> 
\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>\t
\t\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"Washington\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Seattle</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Spokane</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Tacoma</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Vancouver</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Bellevue</option> 
\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>\t
\t\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"West Virginia\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Charleston</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Huntington</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Parkersburg</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Morgantown</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Wheeling</option> 
\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>\t
\t\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"Wisconsin\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Milwaukee</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Madison</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Green Bay</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Kenosha</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Racine</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"Wyoming\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Cheyenne</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Casper</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Laramie</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Gillette</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option>Rock Springs</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t  </form>    
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t<script>
\t\t\t\t\$('#myModal').modal('');
\t\t\t\t</script>
\t\t\t</div>
\t\t</div>
\t\t</div>
\t</div>
\t<div class=\"main-banner banner text-center\">
\t  <div class=\"container\">    
\t\t\t<h1>Sell or Advertise   <span class=\"segment-heading\">    anything online </span> with Resale</h1>
\t\t\t<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
\t\t\t<a href=\"post-ad.html\">Post Free Ad</a>
\t  </div>
\t</div>
\t\t<!-- content-starts-here -->
\t\t<div class=\"content\">
\t\t\t<div class=\"categories\">
\t\t\t\t<div class=\"container\">
\t\t\t\t\t<div class=\"col-md-2 focus-grid\">
\t\t\t\t\t\t<a href=\"categories.html\">
\t\t\t\t\t\t\t<div class=\"focus-border\">
\t\t\t\t\t\t\t\t<div class=\"focus-layout\">
\t\t\t\t\t\t\t\t\t<div class=\"focus-image\"><i class=\"fa fa-mobile\"></i></div>
\t\t\t\t\t\t\t\t\t<h4 class=\"clrchg\">Mobiles</h4>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</a>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-2 focus-grid\">
\t\t\t\t\t\t<a href=\"categories.html#parentVerticalTab2\">
\t\t\t\t\t\t\t<div class=\"focus-border\">
\t\t\t\t\t\t\t\t<div class=\"focus-layout\">
\t\t\t\t\t\t\t\t\t<div class=\"focus-image\"><i class=\"fa fa-laptop\"></i></div>
\t\t\t\t\t\t\t\t\t<h4 class=\"clrchg\"> Electronics & Appliances</h4>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</a>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-2 focus-grid\">
\t\t\t\t\t\t<a href=\"categories.html#parentVerticalTab3\">
\t\t\t\t\t\t\t<div class=\"focus-border\">
\t\t\t\t\t\t\t\t<div class=\"focus-layout\">
\t\t\t\t\t\t\t\t\t<div class=\"focus-image\"><i class=\"fa fa-car\"></i></div>
\t\t\t\t\t\t\t\t\t<h4 class=\"clrchg\">Cars</h4>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</a>
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-md-2 focus-grid\">
\t\t\t\t\t\t<a href=\"categories.html#parentVerticalTab4\">
\t\t\t\t\t\t\t<div class=\"focus-border\">
\t\t\t\t\t\t\t\t<div class=\"focus-layout\">
\t\t\t\t\t\t\t\t\t<div class=\"focus-image\"><i class=\"fa fa-motorcycle\"></i></div>
\t\t\t\t\t\t\t\t\t<h4 class=\"clrchg\">Bikes</h4>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</a>
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-md-2 focus-grid\">
\t\t\t\t\t\t<a href=\"categories.html#parentVerticalTab5\">
\t\t\t\t\t\t\t<div class=\"focus-border\">
\t\t\t\t\t\t\t\t<div class=\"focus-layout\">
\t\t\t\t\t\t\t\t\t<div class=\"focus-image\"><i class=\"fa fa-wheelchair\"></i></div>
\t\t\t\t\t\t\t\t\t<h4 class=\"clrchg\">Furnitures</h4>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</a>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-2 focus-grid\">
\t\t\t\t\t\t<a href=\"categories.html#parentVerticalTab6\">
\t\t\t\t\t\t\t<div class=\"focus-border\">
\t\t\t\t\t\t\t\t<div class=\"focus-layout\">
\t\t\t\t\t\t\t\t\t<div class=\"focus-image\"><i class=\"fa fa-paw\"></i></div>
\t\t\t\t\t\t\t\t\t<h4 class=\"clrchg\">Pets</h4>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</a>
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-md-2 focus-grid\">
\t\t\t\t\t\t<a href=\"categories.html#parentVerticalTab7\">
\t\t\t\t\t\t\t<div class=\"focus-border\">
\t\t\t\t\t\t\t\t<div class=\"focus-layout\">
\t\t\t\t\t\t\t\t\t<div class=\"focus-image\"><i class=\"fa fa-book\"></i></div>
\t\t\t\t\t\t\t\t\t<h4 class=\"clrchg\">Books, Sports & Hobbies</h4>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</a>
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-md-2 focus-grid\">
\t\t\t\t\t\t<a href=\"categories.html#parentVerticalTab8\">
\t\t\t\t\t\t\t<div class=\"focus-border\">
\t\t\t\t\t\t\t\t<div class=\"focus-layout\">
\t\t\t\t\t\t\t\t\t<div class=\"focus-image\"><i class=\"fa fa-asterisk\"></i></div>
\t\t\t\t\t\t\t\t\t<h4 class=\"clrchg\">Fashion</h4>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</a>
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-md-2 focus-grid\">
\t\t\t\t\t\t<a href=\"categories.html#parentVerticalTab9\">
\t\t\t\t\t\t\t<div class=\"focus-border\">
\t\t\t\t\t\t\t\t<div class=\"focus-layout\">
\t\t\t\t\t\t\t\t\t<div class=\"focus-image\"><i class=\"fa fa-gamepad\"></i></div>
\t\t\t\t\t\t\t\t\t<h4 class=\"clrchg\">Kids</h4>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</a>
\t\t\t\t\t</div>\t
\t\t\t\t\t<div class=\"col-md-2 focus-grid\">
\t\t\t\t\t\t<a href=\"categories.html#parentVerticalTab10\">
\t\t\t\t\t\t\t<div class=\"focus-border\">
\t\t\t\t\t\t\t\t<div class=\"focus-layout\">
\t\t\t\t\t\t\t\t\t<div class=\"focus-image\"><i class=\"fa fa-shield\"></i></div>
\t\t\t\t\t\t\t\t\t<h4 class=\"clrchg\">Services</h4>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</a>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-2 focus-grid\">
\t\t\t\t\t\t<a href=\"categories.html#parentVerticalTab11\">
\t\t\t\t\t\t\t<div class=\"focus-border\">
\t\t\t\t\t\t\t\t<div class=\"focus-layout\">
\t\t\t\t\t\t\t\t\t<div class=\"focus-image\"><i class=\"fa fa-at\"></i></div>
\t\t\t\t\t\t\t\t\t<h4 class=\"clrchg\">Jobs</h4>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</a>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-2 focus-grid\">
\t\t\t\t\t\t<a href=\"categories.html#parentVerticalTab12\">
\t\t\t\t\t\t\t<div class=\"focus-border\">
\t\t\t\t\t\t\t\t<div class=\"focus-layout\">
\t\t\t\t\t\t\t\t\t<div class=\"focus-image\"><i class=\"fa fa-home\"></i></div>
\t\t\t\t\t\t\t\t\t<h4 class=\"clrchg\">Real Estate</h4>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</a>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"trending-ads\">
\t\t\t\t<div class=\"container\">
\t\t\t\t<!-- slider -->
\t\t\t\t<div class=\"trend-ads\">
\t\t\t\t\t<h2>Trending Ads</h2>
\t\t\t\t\t\t\t<ul id=\"flexiselDemo3\">
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<div class=\"col-md-3 biseller-column\">
\t\t\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t\t\t<img src=\"images/p1.jpg\"/>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"price\">&#36; 450</span>
\t\t\t\t\t\t\t\t\t\t</a> 
\t\t\t\t\t\t\t\t\t\t<div class=\"ad-info\">
\t\t\t\t\t\t\t\t\t\t\t<h5>There are many variations of passages</h5>
\t\t\t\t\t\t\t\t\t\t\t<span>1 hour ago</span>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"col-md-3 biseller-column\">
\t\t\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t\t\t<img src=\"images/p2.jpg\"/>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"price\">&#36; 399</span>
\t\t\t\t\t\t\t\t\t\t</a> 
\t\t\t\t\t\t\t\t\t\t<div class=\"ad-info\">
\t\t\t\t\t\t\t\t\t\t\t<h5>Lorem Ipsum is simply dummy</h5>
\t\t\t\t\t\t\t\t\t\t\t<span>3 hour ago</span>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"col-md-3 biseller-column\">
\t\t\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t\t\t<img src=\"images/p3.jpg\"/>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"price\">&#36; 199</span>
\t\t\t\t\t\t\t\t\t\t</a> 
\t\t\t\t\t\t\t\t\t\t<div class=\"ad-info\">
\t\t\t\t\t\t\t\t\t\t\t<h5>It is a long established fact that a reader</h5>
\t\t\t\t\t\t\t\t\t\t\t<span>8 hour ago</span>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"col-md-3 biseller-column\">
\t\t\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t\t\t<img src=\"images/p4.jpg\"/>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"price\">&#36; 159</span>
\t\t\t\t\t\t\t\t\t\t</a> 
\t\t\t\t\t\t\t\t\t\t<div class=\"ad-info\">
\t\t\t\t\t\t\t\t\t\t\t<h5>passage of Lorem Ipsum you need to be</h5>
\t\t\t\t\t\t\t\t\t\t\t<span>19 hour ago</span>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<div class=\"col-md-3 biseller-column\">
\t\t\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t\t\t<img src=\"images/p5.jpg\"/>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"price\">&#36; 1599</span>
\t\t\t\t\t\t\t\t\t\t</a> 
\t\t\t\t\t\t\t\t\t\t<div class=\"ad-info\">
\t\t\t\t\t\t\t\t\t\t\t<h5>There are many variations of passages</h5>
\t\t\t\t\t\t\t\t\t\t\t<span>1 hour ago</span>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"col-md-3 biseller-column\">
\t\t\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t\t\t<img src=\"images/p6.jpg\"/>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"price\">&#36; 1099</span>
\t\t\t\t\t\t\t\t\t\t</a> 
\t\t\t\t\t\t\t\t\t\t<div class=\"ad-info\">
\t\t\t\t\t\t\t\t\t\t\t<h5>passage of Lorem Ipsum you need to be</h5>
\t\t\t\t\t\t\t\t\t\t\t<span>1 day ago</span>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"col-md-3 biseller-column\">
\t\t\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t\t\t<img src=\"images/p7.jpg\"/>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"price\">&#36; 109</span>
\t\t\t\t\t\t\t\t\t\t</a> 
\t\t\t\t\t\t\t\t\t\t<div class=\"ad-info\">
\t\t\t\t\t\t\t\t\t\t\t<h5>It is a long established fact that a reader</h5>
\t\t\t\t\t\t\t\t\t\t\t<span>9 hour ago</span>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"col-md-3 biseller-column\">
\t\t\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t\t\t<img src=\"images/p8.jpg\"/>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"price\">&#36; 189</span>
\t\t\t\t\t\t\t\t\t\t</a> 
\t\t\t\t\t\t\t\t\t\t<div class=\"ad-info\">
\t\t\t\t\t\t\t\t\t\t\t<h5>Lorem Ipsum is simply dummy</h5>
\t\t\t\t\t\t\t\t\t\t\t<span>3 hour ago</span>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<div class=\"col-md-3 biseller-column\">
\t\t\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t\t\t<img src=\"images/p9.jpg\"/>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"price\">&#36; 2599</span>
\t\t\t\t\t\t\t\t\t\t</a> 
\t\t\t\t\t\t\t\t\t\t<div class=\"ad-info\">
\t\t\t\t\t\t\t\t\t\t\t<h5>Lorem Ipsum is simply dummy</h5>
\t\t\t\t\t\t\t\t\t\t\t<span>3 hour ago</span>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"col-md-3 biseller-column\">
\t\t\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t\t\t<img src=\"images/p10.jpg\"/>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"price\">&#36; 3999</span>
\t\t\t\t\t\t\t\t\t\t</a> 
\t\t\t\t\t\t\t\t\t\t<div class=\"ad-info\">
\t\t\t\t\t\t\t\t\t\t\t<h5>It is a long established fact that a reader</h5>
\t\t\t\t\t\t\t\t\t\t\t<span>9 hour ago</span>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"col-md-3 biseller-column\">
\t\t\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t\t\t<img src=\"images/p11.jpg\"/>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"price\">&#36; 2699</span>
\t\t\t\t\t\t\t\t\t\t</a> 
\t\t\t\t\t\t\t\t\t\t<div class=\"ad-info\">
\t\t\t\t\t\t\t\t\t\t\t<h5>passage of Lorem Ipsum you need to be</h5>
\t\t\t\t\t\t\t\t\t\t\t<span>1 day ago</span>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"col-md-3 biseller-column\">
\t\t\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t\t\t<img src=\"images/p12.jpg\"/>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"price\">&#36; 899</span>
\t\t\t\t\t\t\t\t\t\t</a> 
\t\t\t\t\t\t\t\t\t\t<div class=\"ad-info\">
\t\t\t\t\t\t\t\t\t\t\t<h5>There are many variations of passages</h5>
\t\t\t\t\t\t\t\t\t\t\t<span>1 hour ago</span>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t<script type=\"text/javascript\">
\t\t\t\t\t\t \$(window).load(function() {
\t\t\t\t\t\t\t\$(\"#flexiselDemo3\").flexisel({
\t\t\t\t\t\t\t\tvisibleItems:1,
\t\t\t\t\t\t\t\tanimationSpeed: 1000,
\t\t\t\t\t\t\t\tautoPlay: true,
\t\t\t\t\t\t\t\tautoPlaySpeed: 5000,    \t\t
\t\t\t\t\t\t\t\tpauseOnHover: true,
\t\t\t\t\t\t\t\tenableResponsiveBreakpoints: true,
\t\t\t\t\t\t\t\tresponsiveBreakpoints: { 
\t\t\t\t\t\t\t\t\tportrait: { 
\t\t\t\t\t\t\t\t\t\tchangePoint:480,
\t\t\t\t\t\t\t\t\t\tvisibleItems:1
\t\t\t\t\t\t\t\t\t}, 
\t\t\t\t\t\t\t\t\tlandscape: { 
\t\t\t\t\t\t\t\t\t\tchangePoint:640,
\t\t\t\t\t\t\t\t\t\tvisibleItems:1
\t\t\t\t\t\t\t\t\t},
\t\t\t\t\t\t\t\t\ttablet: { 
\t\t\t\t\t\t\t\t\t\tchangePoint:768,
\t\t\t\t\t\t\t\t\t\tvisibleItems:1
\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t
\t\t\t\t\t\t});
\t\t\t\t\t   </script>
\t\t\t\t\t   <script type=\"text/javascript\" src=\"js/jquery.flexisel.js\"></script>
\t\t\t\t\t</div>   
\t\t\t</div>
\t\t\t<!-- //slider -->\t\t\t\t
\t\t\t</div>
\t\t\t<div class=\"mobile-app\">
\t\t\t\t<div class=\"container\">
\t\t\t\t\t<div class=\"col-md-5 app-left\">
\t\t\t\t\t\t<a href=\"mobileapp.html\"><img src=\"images/app.png\" alt=\"\"></a>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-7 app-right\">
\t\t\t\t\t\t<h3>Resale App is the <span>Easiest</span> way for Selling and buying second-hand goods</h3>
\t\t\t\t\t\t<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam auctor Sed bibendum varius euismod. Integer eget turpis sit amet lorem rutrum ullamcorper sed sed dui. vestibulum odio at elementum. Suspendisse et condimentum nibh.</p>
\t\t\t\t\t\t<div class=\"app-buttons\">
\t\t\t\t\t\t\t<div class=\"app-button\">
\t\t\t\t\t\t\t\t<a href=\"#\"><img src=\"images/1.png\" alt=\"\"></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"app-button\">
\t\t\t\t\t\t\t\t<a href=\"#\"><img src=\"images/2.png\" alt=\"\"></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"clearfix\"> </div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t<!--footer section start-->\t\t
\t\t<footer>
\t\t\t<div class=\"footer-top\">
\t\t\t\t<div class=\"container\">
\t\t\t\t\t<div class=\"foo-grids\">
\t\t\t\t\t\t<div class=\"col-md-3 footer-grid\">
\t\t\t\t\t\t\t<h4 class=\"footer-head\">Who We Are</h4>
\t\t\t\t\t\t\t<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
\t\t\t\t\t\t\t<p>The point of using Lorem Ipsum is that it has a more-or-less normal letters, as opposed to using 'Content here.</p>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"col-md-3 footer-grid\">
\t\t\t\t\t\t\t<h4 class=\"footer-head\">Help</h4>
\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t<li><a href=\"howitworks.html\">How it Works</a></li>\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<li><a href=\"sitemap.html\">Sitemap</a></li>
\t\t\t\t\t\t\t\t<li><a href=\"faq.html\">Faq</a></li>
\t\t\t\t\t\t\t\t<li><a href=\"feedback.html\">Feedback</a></li>
\t\t\t\t\t\t\t\t<li><a href=\"contact.html\">Contact</a></li>
\t\t\t\t\t\t\t\t<li><a href=\"typography.html\">Shortcodes</a></li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"col-md-3 footer-grid\">
\t\t\t\t\t\t\t<h4 class=\"footer-head\">Information</h4>
\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t<li><a href=\"regions.html\">Locations Map</a></li>\t
\t\t\t\t\t\t\t\t<li><a href=\"terms.html\">Terms of Use</a></li>
\t\t\t\t\t\t\t\t<li><a href=\"popular-search.html\">Popular searches</a></li>\t
\t\t\t\t\t\t\t\t<li><a href=\"privacy.html\">Privacy Policy</a></li>\t
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"col-md-3 footer-grid\">
\t\t\t\t\t\t\t<h4 class=\"footer-head\">Contact Us</h4>
\t\t\t\t\t\t\t<span class=\"hq\">Our headquarters</span>
\t\t\t\t\t\t\t<address>
\t\t\t\t\t\t\t\t<ul class=\"location\">
\t\t\t\t\t\t\t\t\t<li><span class=\"glyphicon glyphicon-map-marker\"></span></li>
\t\t\t\t\t\t\t\t\t<li>CENTER FOR FINANCIAL ASSISTANCE TO DEPOSED NIGERIAN ROYALTY</li>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t</ul>\t
\t\t\t\t\t\t\t\t<ul class=\"location\">
\t\t\t\t\t\t\t\t\t<li><span class=\"glyphicon glyphicon-earphone\"></span></li>
\t\t\t\t\t\t\t\t\t<li>+0 561 111 235</li>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t</ul>\t
\t\t\t\t\t\t\t\t<ul class=\"location\">
\t\t\t\t\t\t\t\t\t<li><span class=\"glyphicon glyphicon-envelope\"></span></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"mailto:info@example.com\">mail@example.com</a></li>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t</ul>\t\t\t\t\t\t
\t\t\t\t\t\t\t</address>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t</div>\t\t\t\t\t\t
\t\t\t\t</div>\t
\t\t\t</div>\t
\t\t\t<div class=\"footer-bottom text-center\">
\t\t\t<div class=\"container\">
\t\t\t\t<div class=\"footer-logo\">
\t\t\t\t\t<a href=\"index.html\"><span>Re</span>sale</a>
\t\t\t\t</div>
\t\t\t\t<div class=\"footer-social-icons\">
\t\t\t\t\t<ul>
\t\t\t\t\t\t<li><a class=\"facebook\" href=\"#\"><span>Facebook</span></a></li>
\t\t\t\t\t\t<li><a class=\"twitter\" href=\"#\"><span>Twitter</span></a></li>
\t\t\t\t\t\t<li><a class=\"flickr\" href=\"#\"><span>Flickr</span></a></li>
\t\t\t\t\t\t<li><a class=\"googleplus\" href=\"#\"><span>Google+</span></a></li>
\t\t\t\t\t\t<li><a class=\"dribbble\" href=\"#\"><span>Dribbble</span></a></li>
\t\t\t\t\t</ul>
\t\t\t\t</div>
\t\t\t\t<div class=\"copyrights\">
\t\t\t\t\t<p> © 2016 Resale. All Rights Reserved | Design by  <a href=\"http://w3layouts.com/\"> W3layouts</a></p>
\t\t\t\t</div>
\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t</div>
\t\t</div>
\t\t</footer>
        <!--footer section end-->
</body>
";
    }

    public function getTemplateName()
    {
        return "CrowdriseIdeesBundle::index.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  114 => 75,  111 => 74,  39 => 4,  36 => 3,  32 => 885,  30 => 74,  27 => 73,  25 => 3,  21 => 1,);
    }
}
