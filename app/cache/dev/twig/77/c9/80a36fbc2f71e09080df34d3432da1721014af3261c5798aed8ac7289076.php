<?php

/* CrowdriseMembreBundle::layout.html.twig */
class __TwigTemplate_77c980a36fbc2f71e09080df34d3432da1721014af3261c5798aed8ac7289076 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'linkcss' => array($this, 'block_linkcss'),
            'js' => array($this, 'block_js'),
            'header' => array($this, 'block_header'),
            'container' => array($this, 'block_container'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
";
        // line 3
        $this->displayBlock('head', $context, $blocks);
        // line 80
        echo "
    
<body>
";
        // line 83
        $this->displayBlock('header', $context, $blocks);
        // line 103
        echo "
        ";
        // line 104
        $this->displayBlock('container', $context, $blocks);
        // line 340
        echo "
\t\t<!--footer section start-->\t
                
                ";
        // line 343
        $this->displayBlock('footer', $context, $blocks);
        // line 421
        echo "
        <!--footer section end-->
</body>
</html>";
    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        // line 4
        echo "<head>
    
";
        // line 6
        $this->displayBlock('title', $context, $blocks);
        // line 11
        echo "
";
        // line 12
        $this->displayBlock('linkcss', $context, $blocks);
        // line 24
        echo "
<!-- for-mobile-apps -->
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
<meta name=\"keywords\" content=\"Resale Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design\" />
<script type=\"application/x-javascript\"> addEventListener(\"load\", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<!--fonts-->
<link href='//fonts.googleapis.com/css?family=Ubuntu+Condensed' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<!--//fonts-->\t
<!-- js -->

";
        // line 38
        $this->displayBlock('js', $context, $blocks);
        // line 76
        echo "

</head>
";
    }

    // line 6
    public function block_title($context, array $blocks = array())
    {
        // line 7
        echo "
<title>Crowdrise</title>

";
    }

    // line 12
    public function block_linkcss($context, array $blocks = array())
    {
        // line 13
        echo "
<link rel=\"stylesheet\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap.min.css"), "html", null, true);
        echo "\">
<link rel=\"stylesheet\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap-select.css"), "html", null, true);
        echo "\">
<link href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
<link rel=\"stylesheet\" href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/flexslider.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"screen\" />
<link rel=\"stylesheet\" href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/font-awesome.min.css"), "html", null, true);
        echo "\" />
<link href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/jquery.uls.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"/>
<link href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/jquery.uls.grid.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"/>
<link href=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/jquery.uls.lcd.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"/>

";
    }

    // line 38
    public function block_js($context, array $blocks = array())
    {
        // line 39
        echo "
<script type=\"text/javascript\" src=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/bootstrap-select.js"), "html", null, true);
        echo "\"></script>

<script>
  \$(document).ready(function () {
    var mySelect = \$('#first-disabled2');

    \$('#special').on('click', function () {
      mySelect.find('option:selected').prop('disabled', true);
      mySelect.selectpicker('refresh');
    });

    \$('#special2').on('click', function () {
      mySelect.find('option:disabled').prop('disabled', false);
      mySelect.selectpicker('refresh');
    });

    \$('#basic2').selectpicker({
      liveSearch: true,
      maxOptions: 1
    });
  });
</script>

<script type=\"text/javascript\" src=\"";
        // line 65
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.leanModal.min.js"), "html", null, true);
        echo "\"></script>

<script src=\"";
        // line 67
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.uls.data.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 68
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.uls.data.utils.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 69
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.uls.lcd.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 70
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.uls.languagefilter.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 71
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.uls.regionfilter.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 72
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.uls.core.js"), "html", null, true);
        echo "\"></script>


";
    }

    // line 83
    public function block_header($context, array $blocks = array())
    {
        // line 84
        echo "
\t<div class=\"header\">
\t\t<div class=\"container\">
\t\t\t<div class=\"logo\">
\t\t\t\t<a href=\"";
        // line 88
        echo $this->env->getExtension('routing')->getPath("crowdrise_membre_homepage");
        echo "\"><span>Crowd</span>rise</a>
\t\t\t</div>
                        
\t\t\t<div class=\"header-right\">
                            
\t\t\t<a class=\"account\" href=\"";
        // line 93
        echo $this->env->getExtension('routing')->getPath("crowdrise_membre_loginpage");
        echo "\">My Account</a>

                        <span class=\"active uls-trigger\"></span>
                        
        \t        <a class=\"account\" href=\"";
        // line 97
        echo $this->env->getExtension('routing')->getPath("crowdrise_profile_homepage");
        echo "\">Profile</a>

\t\t</div>
\t\t</div>
\t</div>
    ";
    }

    // line 104
    public function block_container($context, array $blocks = array())
    {
        // line 105
        echo "\t<div class=\"main-banner banner text-center\">
\t  <div class=\"container\">    
\t\t\t<h1>Keep up your idea   <span class=\"segment-heading\"> , Keep up </span>  your work.</h1>
\t\t\t<p>Crowdrise is the best site for ...</p>
\t\t\t<a href=\"";
        // line 109
        echo $this->env->getExtension('routing')->getPath("crowdrise_membre_loginpage");
        echo "\">Get started</a>
\t  </div>
\t</div>
\t\t<!-- content-starts-here -->
\t\t<div class=\"content\">
\t\t\t<div class=\"categories\">
\t\t\t\t<div class=\"container\">
\t\t\t\t\t<div class=\"col-md-2 focus-grid\">
\t\t\t\t\t\t<a href=\"";
        // line 117
        echo $this->env->getExtension('routing')->getPath("crowdrise_idees_homepage");
        echo "\">
\t\t\t\t\t\t\t<div class=\"focus-border\">
\t\t\t\t\t\t\t\t<div class=\"focus-layout\">
\t\t\t\t\t\t\t\t\t<div class=\"focus-image\"><i class=\"fa fa-mobile\"></i></div>
\t\t\t\t\t\t\t\t\t<h4 class=\"clrchg\">Ideas</h4>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</a>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"col-md-2 focus-grid\">
\t\t\t\t\t\t<a href=\"";
        // line 128
        echo $this->env->getExtension('routing')->getPath("crowdrise_projets_homepage");
        echo "\">
\t\t\t\t\t\t\t<div class=\"focus-border\">
\t\t\t\t\t\t\t\t<div class=\"focus-layout\">
\t\t\t\t\t\t\t\t\t<div class=\"focus-image\"><i class=\"fa fa-car\"></i></div>
\t\t\t\t\t\t\t\t\t<h4 class=\"clrchg\">Projects</h4>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</a>
\t\t\t\t\t</div>\t
\t\t\t\t\t
\t\t\t\t\t<div class=\"col-md-2 focus-grid\">
\t\t\t\t\t\t<a href=\"";
        // line 139
        echo $this->env->getExtension('routing')->getPath("crowdrise_problemes_homepage");
        echo "\">
\t\t\t\t\t\t\t<div class=\"focus-border\">
\t\t\t\t\t\t\t\t<div class=\"focus-layout\">
\t\t\t\t\t\t\t\t\t<div class=\"focus-image\"><i class=\"fa fa-paw\"></i></div>
\t\t\t\t\t\t\t\t\t<h4 class=\"clrchg\">Stack'Probs</h4>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</a>
\t\t\t\t\t</div>\t
\t\t\t\t\t
\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"trending-ads\">
\t\t\t\t<div class=\"container\">
\t\t\t\t<!-- slider -->
\t\t\t\t<div class=\"trend-ads\">
\t\t\t\t\t<h2>Last work</h2>
\t\t\t\t\t\t\t<ul id=\"flexiselDemo3\">
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<div class=\"col-md-3 biseller-column\">
\t\t\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 161
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/p1.jpg"), "html", null, true);
        echo "\"/>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"price\">&#36; 450</span>
\t\t\t\t\t\t\t\t\t\t</a> 
\t\t\t\t\t\t\t\t\t\t<div class=\"ad-info\">
\t\t\t\t\t\t\t\t\t\t\t<h5>There are many variations of passages</h5>
\t\t\t\t\t\t\t\t\t\t\t<span>1 hour ago</span>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"col-md-3 biseller-column\">
\t\t\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 171
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/p2.jpg"), "html", null, true);
        echo "\"/>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"price\">&#36; 399</span>
\t\t\t\t\t\t\t\t\t\t</a> 
\t\t\t\t\t\t\t\t\t\t<div class=\"ad-info\">
\t\t\t\t\t\t\t\t\t\t\t<h5>Lorem Ipsum is simply dummy</h5>
\t\t\t\t\t\t\t\t\t\t\t<span>3 hour ago</span>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"col-md-3 biseller-column\">
\t\t\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 181
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/p3.jpg"), "html", null, true);
        echo "\"/>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"price\">&#36; 199</span>
\t\t\t\t\t\t\t\t\t\t</a> 
\t\t\t\t\t\t\t\t\t\t<div class=\"ad-info\">
\t\t\t\t\t\t\t\t\t\t\t<h5>It is a long established fact that a reader</h5>
\t\t\t\t\t\t\t\t\t\t\t<span>8 hour ago</span>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"col-md-3 biseller-column\">
\t\t\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 191
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/p4.jpg"), "html", null, true);
        echo "\"/>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"price\">&#36; 159</span>
\t\t\t\t\t\t\t\t\t\t</a> 
\t\t\t\t\t\t\t\t\t\t<div class=\"ad-info\">
\t\t\t\t\t\t\t\t\t\t\t<h5>passage of Lorem Ipsum you need to be</h5>
\t\t\t\t\t\t\t\t\t\t\t<span>19 hour ago</span>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<div class=\"col-md-3 biseller-column\">
\t\t\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 203
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/p5.jpg"), "html", null, true);
        echo "\"/>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"price\">&#36; 1599</span>
\t\t\t\t\t\t\t\t\t\t</a> 
\t\t\t\t\t\t\t\t\t\t<div class=\"ad-info\">
\t\t\t\t\t\t\t\t\t\t\t<h5>There are many variations of passages</h5>
\t\t\t\t\t\t\t\t\t\t\t<span>1 hour ago</span>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"col-md-3 biseller-column\">
\t\t\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 213
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/p6.jpg"), "html", null, true);
        echo "\"/>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"price\">&#36; 1099</span>
\t\t\t\t\t\t\t\t\t\t</a> 
\t\t\t\t\t\t\t\t\t\t<div class=\"ad-info\">
\t\t\t\t\t\t\t\t\t\t\t<h5>passage of Lorem Ipsum you need to be</h5>
\t\t\t\t\t\t\t\t\t\t\t<span>1 day ago</span>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"col-md-3 biseller-column\">
\t\t\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 223
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/p7.jpg"), "html", null, true);
        echo "\"/>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"price\">&#36; 109</span>
\t\t\t\t\t\t\t\t\t\t</a> 
\t\t\t\t\t\t\t\t\t\t<div class=\"ad-info\">
\t\t\t\t\t\t\t\t\t\t\t<h5>It is a long established fact that a reader</h5>
\t\t\t\t\t\t\t\t\t\t\t<span>9 hour ago</span>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"col-md-3 biseller-column\">
\t\t\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 233
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/p8.jpg"), "html", null, true);
        echo "\"/>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"price\">&#36; 189</span>
\t\t\t\t\t\t\t\t\t\t</a> 
\t\t\t\t\t\t\t\t\t\t<div class=\"ad-info\">
\t\t\t\t\t\t\t\t\t\t\t<h5>Lorem Ipsum is simply dummy</h5>
\t\t\t\t\t\t\t\t\t\t\t<span>3 hour ago</span>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<div class=\"col-md-3 biseller-column\">
\t\t\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 245
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/p9.jpg"), "html", null, true);
        echo "\"/>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"price\">&#36; 2599</span>
\t\t\t\t\t\t\t\t\t\t</a> 
\t\t\t\t\t\t\t\t\t\t<div class=\"ad-info\">
\t\t\t\t\t\t\t\t\t\t\t<h5>Lorem Ipsum is simply dummy</h5>
\t\t\t\t\t\t\t\t\t\t\t<span>3 hour ago</span>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"col-md-3 biseller-column\">
\t\t\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 255
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/p10.jpg"), "html", null, true);
        echo "\"/>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"price\">&#36; 3999</span>
\t\t\t\t\t\t\t\t\t\t</a> 
\t\t\t\t\t\t\t\t\t\t<div class=\"ad-info\">
\t\t\t\t\t\t\t\t\t\t\t<h5>It is a long established fact that a reader</h5>
\t\t\t\t\t\t\t\t\t\t\t<span>9 hour ago</span>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"col-md-3 biseller-column\">
\t\t\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 265
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/p11.jpg"), "html", null, true);
        echo "\"/>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"price\">&#36; 2699</span>
\t\t\t\t\t\t\t\t\t\t</a> 
\t\t\t\t\t\t\t\t\t\t<div class=\"ad-info\">
\t\t\t\t\t\t\t\t\t\t\t<h5>passage of Lorem Ipsum you need to be</h5>
\t\t\t\t\t\t\t\t\t\t\t<span>1 day ago</span>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"col-md-3 biseller-column\">
\t\t\t\t\t\t\t\t\t\t<a href=\"single.html\">
\t\t\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 275
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/p12.jpg"), "html", null, true);
        echo "\"/>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"price\">&#36; 899</span>
\t\t\t\t\t\t\t\t\t\t</a> 
\t\t\t\t\t\t\t\t\t\t<div class=\"ad-info\">
\t\t\t\t\t\t\t\t\t\t\t<h5>There are many variations of passages</h5>
\t\t\t\t\t\t\t\t\t\t\t<span>1 hour ago</span>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t<script type=\"text/javascript\">
\t\t\t\t\t\t \$(window).load(function() {
\t\t\t\t\t\t\t\$(\"#flexiselDemo3\").flexisel({
\t\t\t\t\t\t\t\tvisibleItems:1,
\t\t\t\t\t\t\t\tanimationSpeed: 1000,
\t\t\t\t\t\t\t\tautoPlay: true,
\t\t\t\t\t\t\t\tautoPlaySpeed: 5000,    \t\t
\t\t\t\t\t\t\t\tpauseOnHover: true,
\t\t\t\t\t\t\t\tenableResponsiveBreakpoints: true,
\t\t\t\t\t\t\t\tresponsiveBreakpoints: { 
\t\t\t\t\t\t\t\t\tportrait: { 
\t\t\t\t\t\t\t\t\t\tchangePoint:480,
\t\t\t\t\t\t\t\t\t\tvisibleItems:1
\t\t\t\t\t\t\t\t\t}, 
\t\t\t\t\t\t\t\t\tlandscape: { 
\t\t\t\t\t\t\t\t\t\tchangePoint:640,
\t\t\t\t\t\t\t\t\t\tvisibleItems:1
\t\t\t\t\t\t\t\t\t},
\t\t\t\t\t\t\t\t\ttablet: { 
\t\t\t\t\t\t\t\t\t\tchangePoint:768,
\t\t\t\t\t\t\t\t\t\tvisibleItems:1
\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t
\t\t\t\t\t\t});
\t\t\t\t\t   </script>
\t\t\t\t\t   <script type=\"text/javascript\" src=\"";
        // line 312
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.flexisel.js"), "html", null, true);
        echo "\"></script>
\t\t\t\t\t</div>   
\t\t\t</div>
\t\t\t<!-- //slider -->\t\t\t\t
\t\t\t</div>
\t\t\t<div class=\"mobile-app\">
\t\t\t\t<div class=\"container\">
\t\t\t\t\t<div class=\"col-md-5 app-left\">
\t\t\t\t\t\t<a href=\"mobileapp.html\"><img src=\"";
        // line 320
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/app.png"), "html", null, true);
        echo "\" alt=\"\"></a>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-7 app-right\">
\t\t\t\t\t\t<h3>Resale App is the <span>Easiest</span> way for Selling and buying second-hand goods</h3>
\t\t\t\t\t\t<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam auctor Sed bibendum varius euismod. Integer eget turpis sit amet lorem rutrum ullamcorper sed sed dui. vestibulum odio at elementum. Suspendisse et condimentum nibh.</p>
\t\t\t\t\t\t<div class=\"app-buttons\">
\t\t\t\t\t\t\t<div class=\"app-button\">
\t\t\t\t\t\t\t\t<a href=\"#\"><img src=\"";
        // line 327
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/1.png"), "html", null, true);
        echo "\" alt=\"\"></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"app-button\">
\t\t\t\t\t\t\t\t<a href=\"#\"><img src=\"";
        // line 330
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/2.png"), "html", null, true);
        echo "\" alt=\"\"></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"clearfix\"> </div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
                ";
    }

    // line 343
    public function block_footer($context, array $blocks = array())
    {
        // line 344
        echo "
\t\t<footer>
\t\t\t<div class=\"footer-top\">
\t\t\t\t<div class=\"container\">
\t\t\t\t\t<div class=\"foo-grids\">
\t\t\t\t\t\t<div class=\"col-md-3 footer-grid\">
\t\t\t\t\t\t\t<h4 class=\"footer-head\">Who We Are</h4>
\t\t\t\t\t\t\t<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
\t\t\t\t\t\t\t<p>The point of using Lorem Ipsum is that it has a more-or-less normal letters, as opposed to using 'Content here.</p>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"col-md-3 footer-grid\">
\t\t\t\t\t\t\t<h4 class=\"footer-head\">Help</h4>
\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t<li><a href=\"howitworks.html\">How it Works</a></li>\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<li><a href=\"sitemap.html\">Sitemap</a></li>
\t\t\t\t\t\t\t\t<li><a href=\"faq.html\">Faq</a></li>
\t\t\t\t\t\t\t\t<li><a href=\"feedback.html\">Feedback</a></li>
\t\t\t\t\t\t\t\t<li><a href=\"contact.html\">Contact</a></li>
\t\t\t\t\t\t\t\t<li><a href=\"typography.html\">Shortcodes</a></li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"col-md-3 footer-grid\">
\t\t\t\t\t\t\t<h4 class=\"footer-head\">Information</h4>
\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t<li><a href=\"regions.html\">Locations Map</a></li>\t
\t\t\t\t\t\t\t\t<li><a href=\"terms.html\">Terms of Use</a></li>
\t\t\t\t\t\t\t\t<li><a href=\"popular-search.html\">Popular searches</a></li>\t
\t\t\t\t\t\t\t\t<li><a href=\"privacy.html\">Privacy Policy</a></li>\t
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"col-md-3 footer-grid\">
\t\t\t\t\t\t\t<h4 class=\"footer-head\">Contact Us</h4>
\t\t\t\t\t\t\t<span class=\"hq\">Our headquarters</span>
\t\t\t\t\t\t\t<address>
\t\t\t\t\t\t\t\t<ul class=\"location\">
\t\t\t\t\t\t\t\t\t<li><span class=\"glyphicon glyphicon-map-marker\"></span></li>
\t\t\t\t\t\t\t\t\t<li>CENTER FOR FINANCIAL ASSISTANCE TO DEPOSED NIGERIAN ROYALTY</li>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t</ul>\t
\t\t\t\t\t\t\t\t<ul class=\"location\">
\t\t\t\t\t\t\t\t\t<li><span class=\"glyphicon glyphicon-earphone\"></span></li>
\t\t\t\t\t\t\t\t\t<li>+0 561 111 235</li>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t</ul>\t
\t\t\t\t\t\t\t\t<ul class=\"location\">
\t\t\t\t\t\t\t\t\t<li><span class=\"glyphicon glyphicon-envelope\"></span></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"mailto:info@example.com\">mail@example.com</a></li>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t</ul>\t\t\t\t\t\t
\t\t\t\t\t\t\t</address>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t</div>\t\t\t\t\t\t
\t\t\t\t</div>\t
\t\t\t</div>\t
\t\t\t<div class=\"footer-bottom text-center\">
\t\t\t<div class=\"container\">
\t\t\t\t<div class=\"footer-logo\">
\t\t\t\t\t<a href=\"index.html\"><span>Re</span>sale</a>
\t\t\t\t</div>
\t\t\t\t<div class=\"footer-social-icons\">
\t\t\t\t\t<ul>
\t\t\t\t\t\t<li><a class=\"facebook\" href=\"#\"><span>Facebook</span></a></li>
\t\t\t\t\t\t<li><a class=\"twitter\" href=\"#\"><span>Twitter</span></a></li>
\t\t\t\t\t\t<li><a class=\"flickr\" href=\"#\"><span>Flickr</span></a></li>
\t\t\t\t\t\t<li><a class=\"googleplus\" href=\"#\"><span>Google+</span></a></li>
\t\t\t\t\t\t<li><a class=\"dribbble\" href=\"#\"><span>Dribbble</span></a></li>
\t\t\t\t\t</ul>
\t\t\t\t</div>
\t\t\t\t<div class=\"copyrights\">
\t\t\t\t\t<p> © 2016 Esprit. All Rights Reserved | Developed by  <a href=\"http://w3layouts.com/\"> DevX Team</a></p>
\t\t\t\t</div>
\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t</div>
\t\t</div>
\t\t</footer>
               ";
    }

    public function getTemplateName()
    {
        return "CrowdriseMembreBundle::layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  559 => 344,  556 => 343,  542 => 330,  536 => 327,  526 => 320,  515 => 312,  475 => 275,  462 => 265,  449 => 255,  436 => 245,  421 => 233,  408 => 223,  395 => 213,  382 => 203,  367 => 191,  354 => 181,  341 => 171,  328 => 161,  303 => 139,  289 => 128,  275 => 117,  264 => 109,  258 => 105,  255 => 104,  245 => 97,  238 => 93,  230 => 88,  224 => 84,  221 => 83,  213 => 72,  209 => 71,  205 => 70,  201 => 69,  197 => 68,  193 => 67,  188 => 65,  162 => 42,  158 => 41,  154 => 40,  151 => 39,  148 => 38,  141 => 21,  137 => 20,  133 => 19,  129 => 18,  125 => 17,  121 => 16,  117 => 15,  113 => 14,  110 => 13,  107 => 12,  100 => 7,  97 => 6,  90 => 76,  88 => 38,  72 => 24,  70 => 12,  67 => 11,  65 => 6,  61 => 4,  58 => 3,  51 => 421,  49 => 343,  44 => 340,  42 => 104,  39 => 103,  37 => 83,  32 => 80,  30 => 3,  26 => 1,);
    }
}
