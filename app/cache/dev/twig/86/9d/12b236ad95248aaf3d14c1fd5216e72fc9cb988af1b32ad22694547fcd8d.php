<?php

/* CrowdriseAdministrationBundle::layout.html.twig */
class __TwigTemplate_869d12b236ad95248aaf3d14c1fd5216e72fc9cb988af1b32ad22694547fcd8d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'navbar' => array($this, 'block_navbar'),
            'nav_li_dash' => array($this, 'block_nav_li_dash'),
            'nav_li_users' => array($this, 'block_nav_li_users'),
            'nav_li_ideasreq' => array($this, 'block_nav_li_ideasreq'),
            'nav_li_projects' => array($this, 'block_nav_li_projects'),
            'nav_li_claims' => array($this, 'block_nav_li_claims'),
            'pagecontent' => array($this, 'block_pagecontent'),
            'pageheader' => array($this, 'block_pageheader'),
            'breadcrumb' => array($this, 'block_breadcrumb'),
            'nbusers' => array($this, 'block_nbusers'),
            'nbideas' => array($this, 'block_nbideas'),
            'nbprojects' => array($this, 'block_nbprojects'),
            'nbclaims' => array($this, 'block_nbclaims'),
            'recentidasactivity' => array($this, 'block_recentidasactivity'),
            'recentclaimsactivity' => array($this, 'block_recentclaimsactivity'),
            'stat1' => array($this, 'block_stat1'),
            'stat2' => array($this, 'block_stat2'),
            'stat3' => array($this, 'block_stat3'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
    
";
        // line 4
        $this->displayBlock('head', $context, $blocks);
        // line 57
        echo "
<body class=\"sidebar-wide\">


";
        // line 61
        $this->displayBlock('navbar', $context, $blocks);
        // line 79
        echo "


    
<!-- Page container -->
<div class=\"page-container\">
  <!-- Sidebar -->
  <div class=\"sidebar collapse\">
    <div class=\"sidebar-content\">




      <!-- User dropdown -->
      <div class=\"user-menu dropdown\"><a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\"><img src=\"";
        // line 93
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/images/demo/users/face1.png"), "html", null, true);
        echo "\" alt=\"\">
        <div class=\"user-info\">Oussama Aydi <span>Administrator</span></div>
        </a>


        <div class=\"popup dropdown-menu dropdown-menu-right\">
          <div class=\"thumbnail\">
            <div class=\"thumb\"><img alt=\"\" src=\"";
        // line 100
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/images/demo/users/face1.png"), "html", null, true);
        echo "\">
              <div class=\"thumb-options\"><span><a href=\"#\" class=\"btn btn-icon btn-success\"><i class=\"icon-pencil\"></i></a><a href=\"#\" class=\"btn btn-icon btn-success\"><i class=\"icon-remove\"></i></a></span></div>
            </div>
            <div class=\"caption text-center\">
              <h6>Oussama Aydi <small>Administrator</small></h6>
            </div>
          </div>
  
        </div>
      </div>
      <!-- /user dropdown -->



      <!-- Main navigation -->
      <ul class=\"navigation\">
          
       ";
        // line 117
        $this->displayBlock('nav_li_dash', $context, $blocks);
        // line 120
        echo "       
       ";
        // line 121
        $this->displayBlock('nav_li_users', $context, $blocks);
        // line 124
        echo "       
       ";
        // line 125
        $this->displayBlock('nav_li_ideasreq', $context, $blocks);
        // line 128
        echo "       
       ";
        // line 129
        $this->displayBlock('nav_li_projects', $context, $blocks);
        // line 132
        echo "       
       ";
        // line 133
        $this->displayBlock('nav_li_claims', $context, $blocks);
        // line 136
        echo "         
      </ul>
      <!-- /main navigation -->



    </div>
  </div>
  <!-- /sidebar -->



  
";
        // line 149
        $this->displayBlock('pagecontent', $context, $blocks);
        // line 324
        echo "  
  
</div>
<!-- /page container -->

    
</body>
</html>";
    }

    // line 4
    public function block_head($context, array $blocks = array())
    {
        // line 5
        echo "
<head>
<meta charset=\"utf-8\">
<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
<title>Crowdrise Admin</title>



<link href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\">
<link href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/css/londinium-theme.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\">
<link href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/css/styles.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\">
<link href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/css/icons.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\">


<link href=\"http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext\" rel=\"stylesheet\" type=\"text/css\">


<script type=\"text/javascript\" src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js\"></script>
<script type=\"text/javascript\" src=\"http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js\"></script>

<script type=\"text/javascript\" src=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/charts/sparkline.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/forms/uniform.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/forms/select2.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/forms/inputmask.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/forms/autosize.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/forms/inputlimit.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/forms/listbox.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/forms/multiselect.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/forms/validate.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/forms/tags.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/forms/switch.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/forms/uploader/plupload.full.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/forms/uploader/plupload.queue.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/forms/wysihtml5/wysihtml5.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/forms/wysihtml5/toolbar.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/interface/daterangepicker.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/interface/fancybox.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/interface/moment.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/interface/jgrowl.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/interface/datatables.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/interface/colorpicker.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/interface/fullcalendar.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 48
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/interface/timepicker.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 49
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/plugins/interface/collapsible.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 51
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/js/application.js"), "html", null, true);
        echo "\"></script>


</head>

";
    }

    // line 61
    public function block_navbar($context, array $blocks = array())
    {
        // line 62
        echo "
<!-- Navbar -->
<div class=\"navbar navbar-inverse\" role=\"navigation\">
  <div class=\"navbar-header\"><a class=\"navbar-brand\" href=\"";
        // line 65
        echo $this->env->getExtension('routing')->getPath("crowdrise_administration_homepage");
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/images/logo.png"), "html", null, true);
        echo "\" alt=\"Londinium\"></a><a class=\"sidebar-toggle\"><i class=\"icon-paragraph-justify2\"></i></a>
    <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#navbar-icons\"><span class=\"sr-only\">Toggle navbar</span><i class=\"icon-grid3\"></i></button>
    <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".sidebar\"><span class=\"sr-only\">Toggle navigation</span><i class=\"icon-paragraph-justify2\"></i></button>
  </div>
  <ul class=\"nav navbar-nav navbar-right collapse\" id=\"navbar-icons\">

    <li><a href=\"#\"><i class=\"icon-exit\"></i> Logout</a>
    </li>
  </ul>
</div>


<!-- /navbar -->
";
    }

    // line 117
    public function block_nav_li_dash($context, array $blocks = array())
    {
        // line 118
        echo "        <li class=\"active\"><a href=\"";
        echo $this->env->getExtension('routing')->getPath("crowdrise_administration_homepage");
        echo "\"><span>Dashboard</span> <i class=\"icon-screen2\"></i></a></li>
       ";
    }

    // line 121
    public function block_nav_li_users($context, array $blocks = array())
    {
        // line 122
        echo "        <li><a href=\"";
        echo $this->env->getExtension('routing')->getPath("crowdrise_administration_users");
        echo "\"><span>Users</span> <i class=\"icon-users2\"></i></a></li>
        ";
    }

    // line 125
    public function block_nav_li_ideasreq($context, array $blocks = array())
    {
        // line 126
        echo "       <li><a href=\"";
        echo $this->env->getExtension('routing')->getPath("crowdrise_administration_ideas_requests");
        echo "\"><span>Ideas Requests</span> <i class=\"icon-stack\"></i></a> </li>
       ";
    }

    // line 129
    public function block_nav_li_projects($context, array $blocks = array())
    {
        // line 130
        echo "       <li><a href=\"";
        echo $this->env->getExtension('routing')->getPath("crowdrise_administration_projects");
        echo "\"><span>Projects</span> <i class=\"icon-stackoverflow\"></i></a> </li>
       ";
    }

    // line 133
    public function block_nav_li_claims($context, array $blocks = array())
    {
        // line 134
        echo "        <li><a href=\"";
        echo $this->env->getExtension('routing')->getPath("crowdrise_administration_claims");
        echo "\"><span>Claims</span> <i class=\"icon-bubble6\"></i></a> </li>
       ";
    }

    // line 149
    public function block_pagecontent($context, array $blocks = array())
    {
        // line 150
        echo "  <!-- Page content -->
  <div class=\"page-content\">
      
   ";
        // line 153
        $this->displayBlock('pageheader', $context, $blocks);
        // line 169
        echo "
    
    ";
        // line 171
        $this->displayBlock('breadcrumb', $context, $blocks);
        // line 183
        echo "

     <!-- Page statistics -->
    <ul class=\"page-stats list-justified\">
      <li class=\"bg-primary\">
        <div class=\"page-stats-showcase\"> <span>No. of Users</span>
          ";
        // line 189
        $this->displayBlock('nbusers', $context, $blocks);
        // line 190
        echo "        </div>
        <div class=\"bar-default chart\">10,14,8,45,23,41,22,31,19,12, 28, 21, 24, 20</div>
      </li>


      <li class=\"bg-danger\">
        <div class=\"page-stats-showcase\"> <span>No. of Ideas</span>
          ";
        // line 197
        $this->displayBlock('nbideas', $context, $blocks);
        // line 198
        echo "        </div>
        <div class=\"bar-default chart\">10,14,8,45,23,41,22,31,19,12, 28, 21, 24, 20</div>
      </li>


      <li class=\"bg-success\">
        <div class=\"page-stats-showcase\"> <span>No. of Projects</span>
          ";
        // line 205
        $this->displayBlock('nbprojects', $context, $blocks);
        // line 206
        echo "        </div>
        <div class=\"bar-default chart\">10,14,8,45,23,41,22,31,19,12, 28, 21, 24, 20</div>
      </li>


      <li class=\"bg-info\">
        <div class=\"page-stats-showcase\"> <span>No. of Claims</span>
          ";
        // line 213
        $this->displayBlock('nbclaims', $context, $blocks);
        // line 214
        echo "        </div>
        <div class=\"bar-default chart\">10,14,8,45,23,41,22,31,19,12, 28, 21, 24, 20</div>
      </li>


    </ul>
    <!-- /page statistics -->

    
    
    <!-- Page tabs -->
    <div class=\"tabbable page-tabs\">
      <ul class=\"nav nav-tabs\">
        <li class=\"active\"><a href=\"#activity\" data-toggle=\"tab\"><i class=\"icon-file\"></i> Activity</a></li>
        <li><a href=\"#statistics\" data-toggle=\"tab\"><i class=\"icon-bars\"></i> Statistics</a></li>
      </ul>



      <div class=\"tab-content\">


        <!-- First tab -->


        <div class=\"tab-pane active fade in\" id=\"activity\">
 
          <!-- Recent activity -->
          <div class=\"block\">
            <h6 class=\"heading-hr\"><i class=\"icon-file\"></i> Recent Ideas activity</h6>


            <ul class=\"media-list\">

                ";
        // line 248
        $this->displayBlock('recentidasactivity', $context, $blocks);
        // line 257
        echo "

            </ul>
          </div>
                
          
          <div class=\"block\">
            <h6 class=\"heading-hr\"><i class=\"icon-file\"></i> Recent Claims activity</h6>


            <ul class=\"media-list\">

             ";
        // line 269
        $this->displayBlock('recentclaimsactivity', $context, $blocks);
        // line 279
        echo "
            </ul>
          </div>
        </div>
        <!-- /first tab -->
        
        
        
        <!-- Secong tab -->
        
        <div class=\"tab-pane active fade in\" id=\"statistics\">
            
            <div class=\"block\">
            <h6 class=\"heading-hr\"><i class=\"icon-pie\"></i> Types Statistics</h6>
            ";
        // line 293
        $this->displayBlock('stat1', $context, $blocks);
        // line 294
        echo "            </div>
            
            <div class=\"block\">
            <h6 class=\"heading-hr\"><i class=\"icon-stats\"></i> Ideas Categories Statistics</h6>
            ";
        // line 298
        $this->displayBlock('stat2', $context, $blocks);
        // line 299
        echo "            </div>
            
           
            <div class=\"block\">
            <h6 class=\"heading-hr\"><i class=\"icon-stats\"></i> Types Statistics</h6>
            ";
        // line 304
        $this->displayBlock('stat3', $context, $blocks);
        // line 305
        echo "            </div>
        </div>

        <!-- /Secong tab -->



      </div>
    </div>
    <!-- /page tabs -->
    <!-- Footer -->
    <div class=\"footer clearfix\">
      <div class=\"pull-left\">&copy; 2016. Crowdrise Admin by <a href=\"http://themeforest.net/user/Kopyov\">DevX Team</a></div>
      <div class=\"pull-right icons-group\"> <a href=\"#\"><i class=\"icon-screen2\"></i></a> <a href=\"#\"><i class=\"icon-balance\"></i></a> <a href=\"#\"><i class=\"icon-cog3\"></i></a> </div>
    </div>
    <!-- /footer -->
  </div>
  <!-- /page content -->
  ";
    }

    // line 153
    public function block_pageheader($context, array $blocks = array())
    {
        echo "   
       
    <!-- Page header -->
    <div class=\"page-header\">
      <div class=\"page-title\">
        <h3>Dashboard <small>Welcome Administrator. This is your managment space.</small></h3>
      </div>
      <div id=\"reportrange\" class=\"range\">
        <div class=\"visible-xs header-element-toggle\"><a class=\"btn btn-primary btn-icon\"><i class=\"icon-calendar\"></i></a></div>
        <div class=\"date-range\"></div>
        <span class=\"label label-danger\">9</span></div>
    </div>


    <!-- /page header -->
    ";
    }

    // line 171
    public function block_breadcrumb($context, array $blocks = array())
    {
        // line 172
        echo "    <!-- Breadcrumbs line -->
    <div class=\"breadcrumb-line\">
      <ul class=\"breadcrumb\">
        <li><a href=\"index.html\">Home</a></li>
        <li class=\"active\">Dashboard</li>
      </ul>
      <div class=\"visible-xs breadcrumb-toggle\"><a class=\"btn btn-link btn-lg btn-icon\" data-toggle=\"collapse\" data-target=\".breadcrumb-buttons\"><i class=\"icon-menu2\"></i></a></div>

    </div>
    <!-- /breadcrumbs line -->
    ";
    }

    // line 189
    public function block_nbusers($context, array $blocks = array())
    {
        echo "<h2>22</h2>";
    }

    // line 197
    public function block_nbideas($context, array $blocks = array())
    {
        echo "<h2>22</h2>";
    }

    // line 205
    public function block_nbprojects($context, array $blocks = array())
    {
        echo "<h2>22</h2>";
    }

    // line 213
    public function block_nbclaims($context, array $blocks = array())
    {
        echo "<h2>22</h2>";
    }

    // line 248
    public function block_recentidasactivity($context, array $blocks = array())
    {
        // line 249
        echo "                    
              <li class=\"media\"><a class=\"pull-left\" href=\"#\"><img class=\"media-object\" src=\"";
        // line 250
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/images/demo/users/face2.png"), "html", null, true);
        echo "\" alt=\"\"></a>
                <div class=\"media-body\">
                  <div class=\"clearfix\"><a href=\"#\" class=\"media-heading\">Mahmoud tabka</a><span class=\"media-notice\">January 10, 2016 / 10:20 pm</span></div>

                  Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.</div>
              </li>
                ";
    }

    // line 269
    public function block_recentclaimsactivity($context, array $blocks = array())
    {
        // line 270
        echo "
              <li class=\"media\"><a class=\"pull-left\" href=\"#\"><img class=\"media-object\" src=\"";
        // line 271
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/images/demo/users/face2.png"), "html", null, true);
        echo "\" alt=\"\"></a>
                <div class=\"media-body\">
                  <div class=\"clearfix\"><a href=\"#\" class=\"media-heading\">Mahmoud tabka</a><span class=\"media-notice\">January 10, 2016 / 10:20 pm</span></div>

                  Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.</div>
              </li>
          
             ";
    }

    // line 293
    public function block_stat1($context, array $blocks = array())
    {
        echo " ";
    }

    // line 298
    public function block_stat2($context, array $blocks = array())
    {
        echo " ";
    }

    // line 304
    public function block_stat3($context, array $blocks = array())
    {
        echo " ";
    }

    public function getTemplateName()
    {
        return "CrowdriseAdministrationBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  654 => 304,  648 => 298,  642 => 293,  630 => 271,  627 => 270,  624 => 269,  613 => 250,  610 => 249,  607 => 248,  601 => 213,  595 => 205,  589 => 197,  583 => 189,  569 => 172,  566 => 171,  545 => 153,  523 => 305,  521 => 304,  514 => 299,  512 => 298,  506 => 294,  504 => 293,  488 => 279,  486 => 269,  472 => 257,  470 => 248,  434 => 214,  432 => 213,  423 => 206,  421 => 205,  412 => 198,  410 => 197,  401 => 190,  399 => 189,  391 => 183,  389 => 171,  385 => 169,  383 => 153,  378 => 150,  375 => 149,  368 => 134,  365 => 133,  358 => 130,  355 => 129,  348 => 126,  345 => 125,  338 => 122,  335 => 121,  328 => 118,  325 => 117,  305 => 65,  300 => 62,  297 => 61,  287 => 51,  283 => 50,  279 => 49,  275 => 48,  271 => 47,  267 => 46,  263 => 45,  259 => 44,  255 => 43,  251 => 42,  247 => 41,  243 => 40,  239 => 39,  235 => 38,  231 => 37,  227 => 36,  223 => 35,  219 => 34,  215 => 33,  211 => 32,  207 => 31,  203 => 30,  199 => 29,  195 => 28,  191 => 27,  187 => 26,  175 => 17,  171 => 16,  167 => 15,  163 => 14,  152 => 5,  149 => 4,  138 => 324,  136 => 149,  121 => 136,  119 => 133,  116 => 132,  114 => 129,  111 => 128,  109 => 125,  106 => 124,  104 => 121,  101 => 120,  99 => 117,  79 => 100,  69 => 93,  53 => 79,  51 => 61,  45 => 57,  43 => 4,  38 => 1,);
    }
}
