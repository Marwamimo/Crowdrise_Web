<?php

/* CrowdriseAdministrationBundle:Default:claims.html.twig */
class __TwigTemplate_edab91410bd170818383142535fa858336360912d7405a404c569f80e101c438 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("CrowdriseAdministrationBundle::layout.html.twig");

        $this->blocks = array(
            'nav_li_dash' => array($this, 'block_nav_li_dash'),
            'nav_li_claims' => array($this, 'block_nav_li_claims'),
            'pagecontent' => array($this, 'block_pagecontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CrowdriseAdministrationBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_nav_li_dash($context, array $blocks = array())
    {
        // line 4
        echo "<li><a href=\"";
        echo $this->env->getExtension('routing')->getPath("crowdrise_administration_homepage");
        echo "\"><span>Dashboard</span> <i class=\"icon-screen2\"></i></a></li>
";
    }

    // line 7
    public function block_nav_li_claims($context, array $blocks = array())
    {
        // line 8
        echo "<li class=\"active\"><a href=\"";
        echo $this->env->getExtension('routing')->getPath("crowdrise_administration_claims");
        echo "\"><span>Claims</span> <i class=\"icon-bubble6\"></i></a> </li>
";
    }

    // line 11
    public function block_pagecontent($context, array $blocks = array())
    {
        // line 12
        echo "    
    <!-- Page content -->
  <div class=\"page-content\">
    <!-- Page header -->
    <div class=\"page-header\">
      <div class=\"page-title\">
        <h3>List of Claims <small>Claims managment</small></h3>
      </div>
      <div id=\"reportrange\" class=\"range\">
        <div class=\"visible-xs header-element-toggle\"><a class=\"btn btn-primary btn-icon\"><i class=\"icon-calendar\"></i></a></div>
        <div class=\"date-range\"></div>
        <span class=\"label label-danger\">9</span></div>
    </div>
    <!-- /page header -->
    <!-- Breadcrumbs line -->
    <div class=\"breadcrumb-line\">
      <ul class=\"breadcrumb\">
        <li><a href=\"index.html\">Home</a></li>
        <li class=\"active\">List of Claims</li>
      </ul>
      <div class=\"visible-xs breadcrumb-toggle\"><a class=\"btn btn-link btn-lg btn-icon\" data-toggle=\"collapse\" data-target=\".breadcrumb-buttons\"><i class=\"icon-menu2\"></i></a></div>

    </div>
    <!-- /breadcrumbs line -->


    <!-- Invoice list -->
    <div class=\"block\">
      <h6 class=\"heading-hr\"><i class=\"icon-stack\"></i> Claims list</h6>
      <div class=\"datatable-invoices\">
        <table class=\"table table-striped table-bordered\">
          <thead>
            <tr>
              <th class=\"invoice-number\">Claim No</th>
              <th>Description</th>
              <th class=\"invoice-amount\">Username</th>
              <th>Date</th>
              <th>Actions</th>
              <th class=\"invoice-expand text-center\">View</th>
            </tr>
          </thead>
          <tbody>

           ";
        // line 55
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["reclamations"]) ? $context["reclamations"] : $this->getContext($context, "reclamations")));
        foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
            // line 56
            echo "            <tr>
              <td><a href=\"invoice.html\"><strong>";
            // line 57
            echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "idReclamation", array()), "html", null, true);
            echo "</strong></a></td>
              <td>";
            // line 58
            echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "contenuReclamation", array()), "html", null, true);
            echo "</td>
              <td><h6>";
            // line 59
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["r"], "idUtilisateur", array()), "username", array()), "html", null, true);
            echo "</h6></td>
              <td><span class=\"label label-danger\">";
            // line 60
            echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "dateReclamation", array()), "html", null, true);
            echo "</span></td>
             
               <td class=\"text-center\"><div class=\"btn-group\">
               <button type=\"button\" class=\"btn btn-icon btn-success dropdown-toggle\" data-toggle=\"dropdown\"><i class=\"icon-cog4\"></i></button>
               <ul class=\"dropdown-menu icons-right dropdown-menu-right\">
               <li><a href=\"";
            // line 65
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("crowdrise_administration_delete_claim", array("id" => $this->getAttribute($context["r"], "idReclamation", array()))), "html", null, true);
            echo "\"><i class=\"icon-checkmark3\"></i> Delete</a></li>
              <li><a href=\"#\"><i class=\"icon-share2\"></i> Report</a></li>
       
           
                </ul>
              </div></td>

                      
              <td class=\"text-center\"><a data-toggle=\"modal\" role=\"button\" href=\"#default-modal\" class=\"btn btn-default btn-xs btn-icon\"><i class=\"icon-file6\"></i></a></td>
            </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 76
        echo "
           
          </tbody>
        </table>
      </div>
    </div>
    <!-- /invoice list -->


    <!-- Default modal -->



    <div id=\"default-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\">
      <div class=\"modal-dialog modal-lg\">
        <div class=\"modal-content\">
          <div class=\"modal-header\">
            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
            <h4 class=\"modal-title\">Claim 12</h4>
          </div>
          <!-- New invoice template -->
          <div class=\"panel\">
            <div class=\"panel-body\">
             
              <h6>Claim contents</h6>
              This invoice contains a incomplete list of items destroyed by the Federation ship Enterprise on Startdate 5401.6 in an unprovked attacked on a peaceful &amp; wholly scientific mission to Outpost 775.The Romulan people demand immediate compensation for the loss of their Warbird, Shuttle, Cloaking Device, and to a lesser extent thier troops.</div>
          </div>
          <!-- /new invoice template -->
          <div class=\"modal-footer\">
            <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button>
            <button type=\"button\" class=\"btn btn-primary\">Go to Users list</button>
          </div>
        </div>
      </div>
    </div>



    <!-- /default modal -->


    <!-- Footer -->
    <div class=\"footer clearfix\">
      <div class=\"pull-left\">&copy; 2016. Crowdrise Admin by <a href=\"http://themeforest.net/user/Kopyov\">DevX Team</a></div>
      <div class=\"pull-right icons-group\"> <a href=\"#\"><i class=\"icon-screen2\"></i></a> <a href=\"#\"><i class=\"icon-balance\"></i></a> <a href=\"#\"><i class=\"icon-cog3\"></i></a> </div>
    </div>
    <!-- /footer -->
  </div>
  <!-- /page content -->

";
    }

    public function getTemplateName()
    {
        return "CrowdriseAdministrationBundle:Default:claims.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  142 => 76,  125 => 65,  117 => 60,  113 => 59,  109 => 58,  105 => 57,  102 => 56,  98 => 55,  53 => 12,  50 => 11,  43 => 8,  40 => 7,  33 => 4,  30 => 3,);
    }
}
