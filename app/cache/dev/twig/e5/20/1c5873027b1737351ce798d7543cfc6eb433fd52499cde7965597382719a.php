<?php

/* CrowdriseAdministrationBundle:Default:index.html.twig */
class __TwigTemplate_e5201c5873027b1737351ce798d7543cfc6eb433fd52499cde7965597382719a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("CrowdriseAdministrationBundle::layout.html.twig");

        $this->blocks = array(
            'nbusers' => array($this, 'block_nbusers'),
            'nbideas' => array($this, 'block_nbideas'),
            'nbprojects' => array($this, 'block_nbprojects'),
            'nbclaims' => array($this, 'block_nbclaims'),
            'stat1' => array($this, 'block_stat1'),
            'stat2' => array($this, 'block_stat2'),
            'stat3' => array($this, 'block_stat3'),
            'recentidasactivity' => array($this, 'block_recentidasactivity'),
            'recentclaimsactivity' => array($this, 'block_recentclaimsactivity'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CrowdriseAdministrationBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_nbusers($context, array $blocks = array())
    {
        echo "<h2>";
        echo twig_escape_filter($this->env, (isset($context["nbUsers"]) ? $context["nbUsers"] : $this->getContext($context, "nbUsers")), "html", null, true);
        echo "</h2>";
    }

    // line 5
    public function block_nbideas($context, array $blocks = array())
    {
        echo "<h2>";
        echo twig_escape_filter($this->env, (isset($context["nbIdeas"]) ? $context["nbIdeas"] : $this->getContext($context, "nbIdeas")), "html", null, true);
        echo "</h2>";
    }

    // line 7
    public function block_nbprojects($context, array $blocks = array())
    {
        echo "<h2>";
        echo twig_escape_filter($this->env, (isset($context["nbProjects"]) ? $context["nbProjects"] : $this->getContext($context, "nbProjects")), "html", null, true);
        echo "</h2>";
    }

    // line 9
    public function block_nbclaims($context, array $blocks = array())
    {
        echo "<h2>";
        echo twig_escape_filter($this->env, (isset($context["nbClaims"]) ? $context["nbClaims"] : $this->getContext($context, "nbClaims")), "html", null, true);
        echo "</h2>";
    }

    // line 11
    public function block_stat1($context, array $blocks = array())
    {
        echo "     

<script src=\"//code.highcharts.com/4.1.8/highcharts.js\"></script>
<script src=\"//code.highcharts.com/4.1.8/modules/exporting.js\"></script>

<script type=\"text/javascript\">
";
        // line 17
        echo $this->env->getExtension('highcharts_extension')->chart((isset($context["chart"]) ? $context["chart"] : $this->getContext($context, "chart")));
        echo "
</script>

<div id=\"piechart\" style=\"min-width: 400px; height: 400px; margin: 0 auto\"></div>     

";
    }

    // line 24
    public function block_stat2($context, array $blocks = array())
    {
        // line 25
        echo "<script src=\"//code.highcharts.com/4.1.8/highcharts.js\"></script>
<script src=\"//code.highcharts.com/4.1.8/modules/exporting.js\"></script>

<script type=\"text/javascript\">
";
        // line 29
        echo $this->env->getExtension('highcharts_extension')->chart((isset($context["chart2"]) ? $context["chart2"] : $this->getContext($context, "chart2")));
        echo "
</script>

<div id=\"piechart2\" style=\"min-width: 400px; height: 400px; margin: 0 auto\"></div>   

";
    }

    // line 37
    public function block_stat3($context, array $blocks = array())
    {
        // line 38
        echo "<script src=\"//code.highcharts.com/4.1.8/highcharts.js\"></script>
<script src=\"//code.highcharts.com/4.1.8/modules/exporting.js\"></script>

<script type=\"text/javascript\">
";
        // line 42
        echo $this->env->getExtension('highcharts_extension')->chart((isset($context["chartline"]) ? $context["chartline"] : $this->getContext($context, "chartline")));
        echo "
</script>

<div id=\"linechart\" style=\"min-width: 600px; height: 400px; margin: 0 auto\"></div>     


";
    }

    // line 53
    public function block_recentidasactivity($context, array $blocks = array())
    {
        // line 54
        echo "                    
";
        // line 55
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["idees"]) ? $context["idees"] : $this->getContext($context, "idees")));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 56
            echo "
<li class=\"media\"><a class=\"pull-left\" href=\"#\"><img class=\"media-object\" src=\"";
            // line 57
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/images/demo/users/avatar.png"), "html", null, true);
            echo "\" alt=\"\"></a>
<div class=\"media-body\">
    
<div class=\"clearfix\"><a href=\"#\" class=\"media-heading\">";
            // line 60
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["i"], "idUtilisateur", array()), "username", array()), "html", null, true);
            echo "</a><span class=\"media-notice\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "dateDepotIdee", array()), "html", null, true);
            echo "</span></div>

Added a new idea called <b>";
            // line 62
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "intituleIdee", array()), "html", null, true);
            echo "</b> in category : <a>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "categorieIdee", array()), "html", null, true);
            echo "</a>.
</div>
</li>

";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 67
        echo "
";
    }

    // line 72
    public function block_recentclaimsactivity($context, array $blocks = array())
    {
        // line 73
        echo "
";
        // line 74
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["reclamations"]) ? $context["reclamations"] : $this->getContext($context, "reclamations")));
        foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
            // line 75
            echo "
<li class=\"media\"><a class=\"pull-left\" href=\"#\"><img class=\"media-object\" src=\"";
            // line 76
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("assets_admin/images/demo/users/avatar.png"), "html", null, true);
            echo "\" alt=\"\"></a>
<div class=\"media-body\">
    
<div class=\"clearfix\"><a href=\"#\" class=\"media-heading\">";
            // line 79
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["r"], "idUtilisateur", array()), "username", array()), "html", null, true);
            echo "</a><span class=\"media-notice\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "dateReclamation", array()), "html", null, true);
            echo "</span></div>

Sent a new Claim.

</div>
</li>

";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 87
        echo "
";
    }

    public function getTemplateName()
    {
        return "CrowdriseAdministrationBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  206 => 87,  190 => 79,  184 => 76,  181 => 75,  177 => 74,  174 => 73,  171 => 72,  166 => 67,  153 => 62,  146 => 60,  140 => 57,  137 => 56,  133 => 55,  130 => 54,  127 => 53,  116 => 42,  110 => 38,  107 => 37,  97 => 29,  91 => 25,  88 => 24,  78 => 17,  68 => 11,  60 => 9,  52 => 7,  44 => 5,  36 => 3,);
    }
}
