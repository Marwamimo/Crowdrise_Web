<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appDevUrlMatcher
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if (rtrim($pathinfo, '/') === '/_profiler') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_profiler_home');
                    }

                    return array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                }

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ($pathinfo === '/_profiler/search') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ($pathinfo === '/_profiler/search_bar') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_purge
                if ($pathinfo === '/_profiler/purge') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:purgeAction',  '_route' => '_profiler_purge',);
                }

                // _profiler_info
                if (0 === strpos($pathinfo, '/_profiler/info') && preg_match('#^/_profiler/info/(?P<about>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_info')), array (  '_controller' => 'web_profiler.controller.profiler:infoAction',));
                }

                // _profiler_phpinfo
                if ($pathinfo === '/_profiler/phpinfo') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            if (0 === strpos($pathinfo, '/_configurator')) {
                // _configurator_home
                if (rtrim($pathinfo, '/') === '/_configurator') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_configurator_home');
                    }

                    return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::checkAction',  '_route' => '_configurator_home',);
                }

                // _configurator_step
                if (0 === strpos($pathinfo, '/_configurator/step') && preg_match('#^/_configurator/step/(?P<index>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_configurator_step')), array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::stepAction',));
                }

                // _configurator_final
                if ($pathinfo === '/_configurator/final') {
                    return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::finalAction',  '_route' => '_configurator_final',);
                }

            }

        }

        // crowdrise_membre_homepage
        if ($pathinfo === '/accueil') {
            return array (  '_controller' => 'Crowdrise\\MembreBundle\\Controller\\DefaultController::indexAction',  '_route' => 'crowdrise_membre_homepage',);
        }

        // crowdrise_membre_loginpage
        if ($pathinfo === '/login') {
            return array (  '_controller' => 'Crowdrise\\MembreBundle\\Controller\\DefaultController::loginAction',  '_route' => 'crowdrise_membre_loginpage',);
        }

        // crowdrise_membre_registerpage
        if ($pathinfo === '/register') {
            return array (  '_controller' => 'Crowdrise\\MembreBundle\\Controller\\DefaultController::registerAction',  '_route' => 'crowdrise_membre_registerpage',);
        }

        // crowdrise_problemes_homepage
        if ($pathinfo === '/stackprobs') {
            return array (  '_controller' => 'Crowdrise\\ProblemesBundle\\Controller\\DefaultController::problemsAction',  '_route' => 'crowdrise_problemes_homepage',);
        }

        // crowdrise_profile_homepage
        if ($pathinfo === '/profile') {
            return array (  '_controller' => 'Crowdrise\\ProfileBundle\\Controller\\DefaultController::profileAction',  '_route' => 'crowdrise_profile_homepage',);
        }

        // crowdrise_idees_homepage
        if ($pathinfo === '/ideas') {
            return array (  '_controller' => 'Crowdrise\\IdeesBundle\\Controller\\DefaultController::someideasAction',  '_route' => 'crowdrise_idees_homepage',);
        }

        // crowdrise_allidees_homepage
        if ($pathinfo === '/allIdeas') {
            return array (  '_controller' => 'Crowdrise\\IdeesBundle\\Controller\\DefaultController::allideasAction',  '_route' => 'crowdrise_allidees_homepage',);
        }

        // crowdrise_projets_homepage
        if ($pathinfo === '/projects') {
            return array (  '_controller' => 'Crowdrise\\ProjetsBundle\\Controller\\DefaultController::projectsAction',  '_route' => 'crowdrise_projets_homepage',);
        }

        if (0 === strpos($pathinfo, '/a')) {
            // crowdrise_allprojets_homepage
            if ($pathinfo === '/allProjects') {
                return array (  '_controller' => 'Crowdrise\\ProjetsBundle\\Controller\\DefaultController::allprojectsAction',  '_route' => 'crowdrise_allprojets_homepage',);
            }

            if (0 === strpos($pathinfo, '/administration_')) {
                // crowdrise_administration_homepage
                if ($pathinfo === '/administration_home') {
                    return array (  '_controller' => 'Crowdrise\\AdministrationBundle\\Controller\\DefaultController::administrationAction',  '_route' => 'crowdrise_administration_homepage',);
                }

                // crowdrise_administration_users
                if ($pathinfo === '/administration_users') {
                    return array (  '_controller' => 'Crowdrise\\AdministrationBundle\\Controller\\UtilisateurController::affichageUtilisateursAction',  '_route' => 'crowdrise_administration_users',);
                }

            }

        }

        // crowdrise_administration_delete_user
        if (0 === strpos($pathinfo, '/deleteuser') && preg_match('#^/deleteuser/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'crowdrise_administration_delete_user')), array (  '_controller' => 'Crowdrise\\AdministrationBundle\\Controller\\UtilisateurController::suppUtilisateurAction',));
        }

        // crowdrise_administration_block_user
        if (0 === strpos($pathinfo, '/blockuser') && preg_match('#^/blockuser/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'crowdrise_administration_block_user')), array (  '_controller' => 'Crowdrise\\AdministrationBundle\\Controller\\UtilisateurController::blockUtilisateurAction',));
        }

        if (0 === strpos($pathinfo, '/a')) {
            // crowdrise_administration_ideas_requests
            if ($pathinfo === '/administration_ideasrequests') {
                return array (  '_controller' => 'Crowdrise\\AdministrationBundle\\Controller\\IdeeController::affichageIdeesAction',  '_route' => 'crowdrise_administration_ideas_requests',);
            }

            // crowdrise_administration_accept_idea
            if (0 === strpos($pathinfo, '/acceptidea') && preg_match('#^/acceptidea/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'crowdrise_administration_accept_idea')), array (  '_controller' => 'Crowdrise\\AdministrationBundle\\Controller\\IdeeController::acceptIdeeAction',));
            }

        }

        // crowdrise_administration_refuse_idea
        if (0 === strpos($pathinfo, '/refuseidea') && preg_match('#^/refuseidea/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'crowdrise_administration_refuse_idea')), array (  '_controller' => 'Crowdrise\\AdministrationBundle\\Controller\\IdeeController::refuseIdeaAction',));
        }

        // crowdrise_administration_claims
        if ($pathinfo === '/administration_claims') {
            return array (  '_controller' => 'Crowdrise\\AdministrationBundle\\Controller\\ReclamationController::affichageReclamationsAction',  '_route' => 'crowdrise_administration_claims',);
        }

        // crowdrise_administration_delete_claim
        if (0 === strpos($pathinfo, '/deleteclaim') && preg_match('#^/deleteclaim/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'crowdrise_administration_delete_claim')), array (  '_controller' => 'Crowdrise\\AdministrationBundle\\Controller\\ReclamationController::suppReclamationAction',));
        }

        // crowdrise_administration_projects
        if ($pathinfo === '/administration_projects') {
            return array (  '_controller' => 'Crowdrise\\AdministrationBundle\\Controller\\ProjetController::affichageProjetsAction',  '_route' => 'crowdrise_administration_projects',);
        }

        // crowdrise_administration_delete_project
        if (0 === strpos($pathinfo, '/deleteproject') && preg_match('#^/deleteproject/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'crowdrise_administration_delete_project')), array (  '_controller' => 'Crowdrise\\AdministrationBundle\\Controller\\ProjetController::suppProjetAction',));
        }

        // _welcome
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', '_welcome');
            }

            return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\WelcomeController::indexAction',  '_route' => '_welcome',);
        }

        if (0 === strpos($pathinfo, '/demo')) {
            if (0 === strpos($pathinfo, '/demo/secured')) {
                if (0 === strpos($pathinfo, '/demo/secured/log')) {
                    if (0 === strpos($pathinfo, '/demo/secured/login')) {
                        // _demo_login
                        if ($pathinfo === '/demo/secured/login') {
                            return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::loginAction',  '_route' => '_demo_login',);
                        }

                        // _demo_security_check
                        if ($pathinfo === '/demo/secured/login_check') {
                            return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::securityCheckAction',  '_route' => '_demo_security_check',);
                        }

                    }

                    // _demo_logout
                    if ($pathinfo === '/demo/secured/logout') {
                        return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::logoutAction',  '_route' => '_demo_logout',);
                    }

                }

                if (0 === strpos($pathinfo, '/demo/secured/hello')) {
                    // acme_demo_secured_hello
                    if ($pathinfo === '/demo/secured/hello') {
                        return array (  'name' => 'World',  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::helloAction',  '_route' => 'acme_demo_secured_hello',);
                    }

                    // _demo_secured_hello
                    if (preg_match('#^/demo/secured/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => '_demo_secured_hello')), array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::helloAction',));
                    }

                    // _demo_secured_hello_admin
                    if (0 === strpos($pathinfo, '/demo/secured/hello/admin') && preg_match('#^/demo/secured/hello/admin/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => '_demo_secured_hello_admin')), array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::helloadminAction',));
                    }

                }

            }

            // _demo
            if (rtrim($pathinfo, '/') === '/demo') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', '_demo');
                }

                return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\DemoController::indexAction',  '_route' => '_demo',);
            }

            // _demo_hello
            if (0 === strpos($pathinfo, '/demo/hello') && preg_match('#^/demo/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_demo_hello')), array (  '_controller' => 'Acme\\DemoBundle\\Controller\\DemoController::helloAction',));
            }

            // _demo_contact
            if ($pathinfo === '/demo/contact') {
                return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\DemoController::contactAction',  '_route' => '_demo_contact',);
            }

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
