<?php

namespace Proxies\__CG__\Crowdrise\AdministrationBundle\Entity;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class Utilisateur extends \Crowdrise\AdministrationBundle\Entity\Utilisateur implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array properties to be lazy loaded, with keys being the property
     *            names and values being their default values
     *
     * @see \Doctrine\Common\Persistence\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = array();



    /**
     * @param \Closure $initializer
     * @param \Closure $cloner
     */
    public function __construct($initializer = null, $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return array('__isInitialized__', '' . "\0" . 'Crowdrise\\AdministrationBundle\\Entity\\Utilisateur' . "\0" . 'idUtilisateur', '' . "\0" . 'Crowdrise\\AdministrationBundle\\Entity\\Utilisateur' . "\0" . 'nomUtilisateur', '' . "\0" . 'Crowdrise\\AdministrationBundle\\Entity\\Utilisateur' . "\0" . 'prenomUtilisateur', '' . "\0" . 'Crowdrise\\AdministrationBundle\\Entity\\Utilisateur' . "\0" . 'username', '' . "\0" . 'Crowdrise\\AdministrationBundle\\Entity\\Utilisateur' . "\0" . 'password', '' . "\0" . 'Crowdrise\\AdministrationBundle\\Entity\\Utilisateur' . "\0" . 'email', '' . "\0" . 'Crowdrise\\AdministrationBundle\\Entity\\Utilisateur' . "\0" . 'note', '' . "\0" . 'Crowdrise\\AdministrationBundle\\Entity\\Utilisateur' . "\0" . 'competences', '' . "\0" . 'Crowdrise\\AdministrationBundle\\Entity\\Utilisateur' . "\0" . 'experience', '' . "\0" . 'Crowdrise\\AdministrationBundle\\Entity\\Utilisateur' . "\0" . 'nbProjets', '' . "\0" . 'Crowdrise\\AdministrationBundle\\Entity\\Utilisateur' . "\0" . 'nbIdees', '' . "\0" . 'Crowdrise\\AdministrationBundle\\Entity\\Utilisateur' . "\0" . 'role', '' . "\0" . 'Crowdrise\\AdministrationBundle\\Entity\\Utilisateur' . "\0" . 'urlImg', '' . "\0" . 'Crowdrise\\AdministrationBundle\\Entity\\Utilisateur' . "\0" . 'bloque');
        }

        return array('__isInitialized__', '' . "\0" . 'Crowdrise\\AdministrationBundle\\Entity\\Utilisateur' . "\0" . 'idUtilisateur', '' . "\0" . 'Crowdrise\\AdministrationBundle\\Entity\\Utilisateur' . "\0" . 'nomUtilisateur', '' . "\0" . 'Crowdrise\\AdministrationBundle\\Entity\\Utilisateur' . "\0" . 'prenomUtilisateur', '' . "\0" . 'Crowdrise\\AdministrationBundle\\Entity\\Utilisateur' . "\0" . 'username', '' . "\0" . 'Crowdrise\\AdministrationBundle\\Entity\\Utilisateur' . "\0" . 'password', '' . "\0" . 'Crowdrise\\AdministrationBundle\\Entity\\Utilisateur' . "\0" . 'email', '' . "\0" . 'Crowdrise\\AdministrationBundle\\Entity\\Utilisateur' . "\0" . 'note', '' . "\0" . 'Crowdrise\\AdministrationBundle\\Entity\\Utilisateur' . "\0" . 'competences', '' . "\0" . 'Crowdrise\\AdministrationBundle\\Entity\\Utilisateur' . "\0" . 'experience', '' . "\0" . 'Crowdrise\\AdministrationBundle\\Entity\\Utilisateur' . "\0" . 'nbProjets', '' . "\0" . 'Crowdrise\\AdministrationBundle\\Entity\\Utilisateur' . "\0" . 'nbIdees', '' . "\0" . 'Crowdrise\\AdministrationBundle\\Entity\\Utilisateur' . "\0" . 'role', '' . "\0" . 'Crowdrise\\AdministrationBundle\\Entity\\Utilisateur' . "\0" . 'urlImg', '' . "\0" . 'Crowdrise\\AdministrationBundle\\Entity\\Utilisateur' . "\0" . 'bloque');
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (Utilisateur $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy->__getLazyProperties() as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', array());
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', array());
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function getIdUtilisateur()
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getIdUtilisateur();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getIdUtilisateur', array());

        return parent::getIdUtilisateur();
    }

    /**
     * {@inheritDoc}
     */
    public function setNomUtilisateur($nomUtilisateur)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setNomUtilisateur', array($nomUtilisateur));

        return parent::setNomUtilisateur($nomUtilisateur);
    }

    /**
     * {@inheritDoc}
     */
    public function getNomUtilisateur()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getNomUtilisateur', array());

        return parent::getNomUtilisateur();
    }

    /**
     * {@inheritDoc}
     */
    public function setPrenomUtilisateur($prenomUtilisateur)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPrenomUtilisateur', array($prenomUtilisateur));

        return parent::setPrenomUtilisateur($prenomUtilisateur);
    }

    /**
     * {@inheritDoc}
     */
    public function getPrenomUtilisateur()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPrenomUtilisateur', array());

        return parent::getPrenomUtilisateur();
    }

    /**
     * {@inheritDoc}
     */
    public function setUsername($username)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setUsername', array($username));

        return parent::setUsername($username);
    }

    /**
     * {@inheritDoc}
     */
    public function getUsername()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getUsername', array());

        return parent::getUsername();
    }

    /**
     * {@inheritDoc}
     */
    public function setPassword($password)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPassword', array($password));

        return parent::setPassword($password);
    }

    /**
     * {@inheritDoc}
     */
    public function getPassword()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPassword', array());

        return parent::getPassword();
    }

    /**
     * {@inheritDoc}
     */
    public function setEmail($email)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setEmail', array($email));

        return parent::setEmail($email);
    }

    /**
     * {@inheritDoc}
     */
    public function getEmail()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getEmail', array());

        return parent::getEmail();
    }

    /**
     * {@inheritDoc}
     */
    public function setNote($note)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setNote', array($note));

        return parent::setNote($note);
    }

    /**
     * {@inheritDoc}
     */
    public function getNote()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getNote', array());

        return parent::getNote();
    }

    /**
     * {@inheritDoc}
     */
    public function setCompetences($competences)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCompetences', array($competences));

        return parent::setCompetences($competences);
    }

    /**
     * {@inheritDoc}
     */
    public function getCompetences()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCompetences', array());

        return parent::getCompetences();
    }

    /**
     * {@inheritDoc}
     */
    public function setExperience($experience)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setExperience', array($experience));

        return parent::setExperience($experience);
    }

    /**
     * {@inheritDoc}
     */
    public function getExperience()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getExperience', array());

        return parent::getExperience();
    }

    /**
     * {@inheritDoc}
     */
    public function setNbProjets($nbProjets)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setNbProjets', array($nbProjets));

        return parent::setNbProjets($nbProjets);
    }

    /**
     * {@inheritDoc}
     */
    public function getNbProjets()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getNbProjets', array());

        return parent::getNbProjets();
    }

    /**
     * {@inheritDoc}
     */
    public function setNbIdees($nbIdees)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setNbIdees', array($nbIdees));

        return parent::setNbIdees($nbIdees);
    }

    /**
     * {@inheritDoc}
     */
    public function getNbIdees()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getNbIdees', array());

        return parent::getNbIdees();
    }

    /**
     * {@inheritDoc}
     */
    public function setRole($role)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setRole', array($role));

        return parent::setRole($role);
    }

    /**
     * {@inheritDoc}
     */
    public function getRole()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getRole', array());

        return parent::getRole();
    }

    /**
     * {@inheritDoc}
     */
    public function setUrlImg($urlImg)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setUrlImg', array($urlImg));

        return parent::setUrlImg($urlImg);
    }

    /**
     * {@inheritDoc}
     */
    public function getUrlImg()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getUrlImg', array());

        return parent::getUrlImg();
    }

    /**
     * {@inheritDoc}
     */
    public function setBloque($bloque)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setBloque', array($bloque));

        return parent::setBloque($bloque);
    }

    /**
     * {@inheritDoc}
     */
    public function getBloque()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getBloque', array());

        return parent::getBloque();
    }

}
