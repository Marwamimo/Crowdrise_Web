<?php

namespace Crowdrise\ProfileBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('CrowdriseProfileBundle:Default:index.html.twig', array('name' => $name));
    }

    public function profileAction()
    {
        return $this->render('CrowdriseProfileBundle:Default:profile.html.twig');
    }
}
