<?php

namespace Crowdrise\AdministrationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Projet
 *
 * @ORM\Table(name="projet", indexes={@ORM\Index(name="fk_projet_utilisateur", columns={"id_utilisateur"})})
 * @ORM\Entity(repositoryClass="Crowdrise\AdministrationBundle\Entity\ProjetRepository")
 */
class Projet
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_projet", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idProjet;

    /**
     * @var string
     *
     * @ORM\Column(name="intitule_projet", type="string", length=30, nullable=false)
     */
    private $intituleProjet;

    /**
     * @var string
     *
     * @ORM\Column(name="categorie_projet", type="string", length=30, nullable=false)
     */
    private $categorieProjet;

    /**
     * @var string
     *
     * @ORM\Column(name="description_projet", type="string", length=500, nullable=false)
     */
    private $descriptionProjet;

    /**
     * @var string
     *
     * @ORM\Column(name="competences_demande_projet", type="string", length=500, nullable=false)
     */
    private $competencesDemandeProjet;

    /**
     * @var string
     *
     * @ORM\Column(name="motcle_projet", type="string", length=30, nullable=false)
     */
    private $motcleProjet;

    /**
     * @var string
     *
     * @ORM\Column(name="date_depot_projet", type="string", length=30, nullable=false)
     */
    private $dateDepotProjet;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbsolvers_projet", type="integer", nullable=false)
     */
    private $nbsolversProjet;

    /**
     * @var \Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_utilisateur", referencedColumnName="id_utilisateur")
     * })
     */
    private $idUtilisateur;



    /**
     * Get idProjet
     *
     * @return integer 
     */
    public function getIdProjet()
    {
        return $this->idProjet;
    }

    /**
     * Set intituleProjet
     *
     * @param string $intituleProjet
     * @return Projet
     */
    public function setIntituleProjet($intituleProjet)
    {
        $this->intituleProjet = $intituleProjet;

        return $this;
    }

    /**
     * Get intituleProjet
     *
     * @return string 
     */
    public function getIntituleProjet()
    {
        return $this->intituleProjet;
    }

    /**
     * Set categorieProjet
     *
     * @param string $categorieProjet
     * @return Projet
     */
    public function setCategorieProjet($categorieProjet)
    {
        $this->categorieProjet = $categorieProjet;

        return $this;
    }

    /**
     * Get categorieProjet
     *
     * @return string 
     */
    public function getCategorieProjet()
    {
        return $this->categorieProjet;
    }

    /**
     * Set descriptionProjet
     *
     * @param string $descriptionProjet
     * @return Projet
     */
    public function setDescriptionProjet($descriptionProjet)
    {
        $this->descriptionProjet = $descriptionProjet;

        return $this;
    }

    /**
     * Get descriptionProjet
     *
     * @return string 
     */
    public function getDescriptionProjet()
    {
        return $this->descriptionProjet;
    }

    /**
     * Set competencesDemandeProjet
     *
     * @param string $competencesDemandeProjet
     * @return Projet
     */
    public function setCompetencesDemandeProjet($competencesDemandeProjet)
    {
        $this->competencesDemandeProjet = $competencesDemandeProjet;

        return $this;
    }

    /**
     * Get competencesDemandeProjet
     *
     * @return string 
     */
    public function getCompetencesDemandeProjet()
    {
        return $this->competencesDemandeProjet;
    }

    /**
     * Set motcleProjet
     *
     * @param string $motcleProjet
     * @return Projet
     */
    public function setMotcleProjet($motcleProjet)
    {
        $this->motcleProjet = $motcleProjet;

        return $this;
    }

    /**
     * Get motcleProjet
     *
     * @return string 
     */
    public function getMotcleProjet()
    {
        return $this->motcleProjet;
    }

    /**
     * Set dateDepotProjet
     *
     * @param string $dateDepotProjet
     * @return Projet
     */
    public function setDateDepotProjet($dateDepotProjet)
    {
        $this->dateDepotProjet = $dateDepotProjet;

        return $this;
    }

    /**
     * Get dateDepotProjet
     *
     * @return string 
     */
    public function getDateDepotProjet()
    {
        return $this->dateDepotProjet;
    }

    /**
     * Set nbsolversProjet
     *
     * @param integer $nbsolversProjet
     * @return Projet
     */
    public function setNbsolversProjet($nbsolversProjet)
    {
        $this->nbsolversProjet = $nbsolversProjet;

        return $this;
    }

    /**
     * Get nbsolversProjet
     *
     * @return integer 
     */
    public function getNbsolversProjet()
    {
        return $this->nbsolversProjet;
    }

    /**
     * Set idUtilisateur
     *
     * @param \Crowdrise\AdministrationBundle\Entity\Utilisateur $idUtilisateur
     * @return Projet
     */
    public function setIdUtilisateur(\Crowdrise\AdministrationBundle\Entity\Utilisateur $idUtilisateur = null)
    {
        $this->idUtilisateur = $idUtilisateur;

        return $this;
    }

    /**
     * Get idUtilisateur
     *
     * @return \Crowdrise\AdministrationBundle\Entity\Utilisateur 
     */
    public function getIdUtilisateur()
    {
        return $this->idUtilisateur;
    }
}
