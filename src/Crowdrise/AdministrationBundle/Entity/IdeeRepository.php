<?php

namespace Crowdrise\AdministrationBundle\Entity;

use Doctrine\ORM\EntityRepository;

class IdeeRepository extends EntityRepository{
    
     public function CountNBIdeas(){
         
          $query=$this->getEntityManager()
                 
                  ->createQuery('SELECT COUNT(i.idIdee) FROM CrowdriseAdministrationBundle:Idee i');
     
      return $result = $query->getSingleScalarResult();
    }
    
    
    public function AcceptIdea($id){
        
            $query=$this->getEntityManager()
                    
                    ->createQuery("UPDATE CrowdriseAdministrationBundle:Idee i SET  i.etatIdee = 'Accept' WHERE i.idIdee = :id");
 
            $query->setParameter('id', $id);
            $query->execute();
}


    public function RefuseIdea($id){
        
            $query=$this->getEntityManager()
                    
                    ->createQuery("UPDATE CrowdriseAdministrationBundle:Idee i SET  i.etatIdee = 'Refusee' WHERE i.idIdee = :id");
 
            $query->setParameter('id', $id);
            $query->execute();
}


}
