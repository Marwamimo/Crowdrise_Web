<?php

namespace Crowdrise\AdministrationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reclamation
 *
 * @ORM\Table(name="reclamation", indexes={@ORM\Index(name="fk_reclamation_utilisateur", columns={"id_utilisateur"})})
 * @ORM\Entity(repositoryClass="Crowdrise\AdministrationBundle\Entity\ReclamationRepository")
 */
class Reclamation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_reclamation", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idReclamation;

    /**
     * @var string
     *
     * @ORM\Column(name="date_reclamation", type="string", length=30, nullable=false)
     */
    private $dateReclamation;

    /**
     * @var string
     *
     * @ORM\Column(name="contenu_reclamation", type="string", length=500, nullable=false)
     */
    private $contenuReclamation;

    /**
     * @var \Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_utilisateur", referencedColumnName="id_utilisateur")
     * })
     */
    private $idUtilisateur;



    /**
     * Get idReclamation
     *
     * @return integer 
     */
    public function getIdReclamation()
    {
        return $this->idReclamation;
    }

    /**
     * Set dateReclamation
     *
     * @param string $dateReclamation
     * @return Reclamation
     */
    public function setDateReclamation($dateReclamation)
    {
        $this->dateReclamation = $dateReclamation;

        return $this;
    }

    /**
     * Get dateReclamation
     *
     * @return string 
     */
    public function getDateReclamation()
    {
        return $this->dateReclamation;
    }

    /**
     * Set contenuReclamation
     *
     * @param string $contenuReclamation
     * @return Reclamation
     */
    public function setContenuReclamation($contenuReclamation)
    {
        $this->contenuReclamation = $contenuReclamation;

        return $this;
    }

    /**
     * Get contenuReclamation
     *
     * @return string 
     */
    public function getContenuReclamation()
    {
        return $this->contenuReclamation;
    }

    /**
     * Set idUtilisateur
     *
     * @param \Crowdrise\AdministrationBundle\Entity\Utilisateur $idUtilisateur
     * @return Reclamation
     */
    public function setIdUtilisateur(\Crowdrise\AdministrationBundle\Entity\Utilisateur $idUtilisateur = null)
    {
        $this->idUtilisateur = $idUtilisateur;

        return $this;
    }

    /**
     * Get idUtilisateur
     *
     * @return \Crowdrise\AdministrationBundle\Entity\Utilisateur 
     */
    public function getIdUtilisateur()
    {
        return $this->idUtilisateur;
    }
}
