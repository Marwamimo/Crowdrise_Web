<?php

namespace Crowdrise\AdministrationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Paiement
 *
 * @ORM\Table(name="paiement", indexes={@ORM\Index(name="fk_utilisateurp", columns={"id_utilisateur_p"}), @ORM\Index(name="fk_utilisateurb", columns={"id_utilisateur_b"})})
 * @ORM\Entity
 */
class Paiement
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_paiement", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idPaiement;

    /**
     * @var integer
     *
     * @ORM\Column(name="montant_paiement", type="integer", nullable=false)
     */
    private $montantPaiement;

    /**
     * @var integer
     *
     * @ORM\Column(name="date_paiement", type="integer", nullable=false)
     */
    private $datePaiement;

    /**
     * @var \Utilisateur
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_utilisateur_b", referencedColumnName="id_utilisateur")
     * })
     */
    private $idUtilisateurB;

    /**
     * @var \Utilisateur
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_utilisateur_p", referencedColumnName="id_utilisateur")
     * })
     */
    private $idUtilisateurP;



    /**
     * Set idPaiement
     *
     * @param integer $idPaiement
     * @return Paiement
     */
    public function setIdPaiement($idPaiement)
    {
        $this->idPaiement = $idPaiement;

        return $this;
    }

    /**
     * Get idPaiement
     *
     * @return integer 
     */
    public function getIdPaiement()
    {
        return $this->idPaiement;
    }

    /**
     * Set montantPaiement
     *
     * @param integer $montantPaiement
     * @return Paiement
     */
    public function setMontantPaiement($montantPaiement)
    {
        $this->montantPaiement = $montantPaiement;

        return $this;
    }

    /**
     * Get montantPaiement
     *
     * @return integer 
     */
    public function getMontantPaiement()
    {
        return $this->montantPaiement;
    }

    /**
     * Set datePaiement
     *
     * @param integer $datePaiement
     * @return Paiement
     */
    public function setDatePaiement($datePaiement)
    {
        $this->datePaiement = $datePaiement;

        return $this;
    }

    /**
     * Get datePaiement
     *
     * @return integer 
     */
    public function getDatePaiement()
    {
        return $this->datePaiement;
    }

    /**
     * Set idUtilisateurB
     *
     * @param \Crowdrise\AdministrationBundle\Entity\Utilisateur $idUtilisateurB
     * @return Paiement
     */
    public function setIdUtilisateurB(\Crowdrise\AdministrationBundle\Entity\Utilisateur $idUtilisateurB)
    {
        $this->idUtilisateurB = $idUtilisateurB;

        return $this;
    }

    /**
     * Get idUtilisateurB
     *
     * @return \Crowdrise\AdministrationBundle\Entity\Utilisateur 
     */
    public function getIdUtilisateurB()
    {
        return $this->idUtilisateurB;
    }

    /**
     * Set idUtilisateurP
     *
     * @param \Crowdrise\AdministrationBundle\Entity\Utilisateur $idUtilisateurP
     * @return Paiement
     */
    public function setIdUtilisateurP(\Crowdrise\AdministrationBundle\Entity\Utilisateur $idUtilisateurP)
    {
        $this->idUtilisateurP = $idUtilisateurP;

        return $this;
    }

    /**
     * Get idUtilisateurP
     *
     * @return \Crowdrise\AdministrationBundle\Entity\Utilisateur 
     */
    public function getIdUtilisateurP()
    {
        return $this->idUtilisateurP;
    }
}
