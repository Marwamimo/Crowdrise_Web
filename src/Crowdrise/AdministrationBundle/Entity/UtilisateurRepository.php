<?php

namespace Crowdrise\AdministrationBundle\Entity;

use Doctrine\ORM\EntityRepository;


class UtilisateurRepository extends EntityRepository{
        
    public function CountNBusers(){
          $query=$this->getEntityManager()
                 
                  ->createQuery('SELECT COUNT(u.idUtilisateur) FROM CrowdriseAdministrationBundle:Utilisateur u');
     
      return $result = $query->getSingleScalarResult();


    }
    
    public function blockUser($id){
        
            $query=$this->getEntityManager()
                    
                    ->createQuery("UPDATE CrowdriseAdministrationBundle:Utilisateur u SET  u.bloque = '1' WHERE u.idUtilisateur = :id");
 
            $query->setParameter('id', $id);
            $query->execute();
}
    
}



