<?php

namespace Crowdrise\AdministrationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Utilisateur
 *
 * @ORM\Table(name="utilisateur")
 * @ORM\Entity(repositoryClass="Crowdrise\AdministrationBundle\Entity\UtilisateurRepository")
 */
class Utilisateur
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_utilisateur", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idUtilisateur;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_utilisateur", type="string", length=30, nullable=false)
     */
    private $nomUtilisateur;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom_utilisateur", type="string", length=30, nullable=false)
     */
    private $prenomUtilisateur;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=30, nullable=false)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=30, nullable=false)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=30, nullable=false)
     */
    private $email;

    /**
     * @var integer
     *
     * @ORM\Column(name="note", type="integer", nullable=true)
     */
    private $note;

    /**
     * @var string
     *
     * @ORM\Column(name="competences", type="string", length=500, nullable=true)
     */
    private $competences;

    /**
     * @var string
     *
     * @ORM\Column(name="experience", type="string", length=200, nullable=true)
     */
    private $experience;

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_projets", type="integer", nullable=true)
     */
    private $nbProjets;

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_idees", type="integer", nullable=true)
     */
    private $nbIdees;

    /**
     * @var string
     *
     * @ORM\Column(name="role", type="string", length=11, nullable=false)
     */
    private $role;

    /**
     * @var string
     *
     * @ORM\Column(name="URL_img", type="string", length=255, nullable=true)
     */
    private $urlImg;
    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="bloque", type="integer", nullable=false)
     */
    private $bloque;



    /**
     * Get idUtilisateur
     *
     * @return integer 
     */
    public function getIdUtilisateur()
    {
        return $this->idUtilisateur;
    }

    /**
     * Set nomUtilisateur
     *
     * @param string $nomUtilisateur
     * @return Utilisateur
     */
    public function setNomUtilisateur($nomUtilisateur)
    {
        $this->nomUtilisateur = $nomUtilisateur;

        return $this;
    }

    /**
     * Get nomUtilisateur
     *
     * @return string 
     */
    public function getNomUtilisateur()
    {
        return $this->nomUtilisateur;
    }

    /**
     * Set prenomUtilisateur
     *
     * @param string $prenomUtilisateur
     * @return Utilisateur
     */
    public function setPrenomUtilisateur($prenomUtilisateur)
    {
        $this->prenomUtilisateur = $prenomUtilisateur;

        return $this;
    }

    /**
     * Get prenomUtilisateur
     *
     * @return string 
     */
    public function getPrenomUtilisateur()
    {
        return $this->prenomUtilisateur;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return Utilisateur
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string 
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return Utilisateur
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Utilisateur
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set note
     *
     * @param integer $note
     * @return Utilisateur
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return integer 
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set competences
     *
     * @param string $competences
     * @return Utilisateur
     */
    public function setCompetences($competences)
    {
        $this->competences = $competences;

        return $this;
    }

    /**
     * Get competences
     *
     * @return string 
     */
    public function getCompetences()
    {
        return $this->competences;
    }

    /**
     * Set experience
     *
     * @param string $experience
     * @return Utilisateur
     */
    public function setExperience($experience)
    {
        $this->experience = $experience;

        return $this;
    }

    /**
     * Get experience
     *
     * @return string 
     */
    public function getExperience()
    {
        return $this->experience;
    }

    /**
     * Set nbProjets
     *
     * @param integer $nbProjets
     * @return Utilisateur
     */
    public function setNbProjets($nbProjets)
    {
        $this->nbProjets = $nbProjets;

        return $this;
    }

    /**
     * Get nbProjets
     *
     * @return integer 
     */
    public function getNbProjets()
    {
        return $this->nbProjets;
    }

    /**
     * Set nbIdees
     *
     * @param integer $nbIdees
     * @return Utilisateur
     */
    public function setNbIdees($nbIdees)
    {
        $this->nbIdees = $nbIdees;

        return $this;
    }

    /**
     * Get nbIdees
     *
     * @return integer 
     */
    public function getNbIdees()
    {
        return $this->nbIdees;
    }

    /**
     * Set role
     *
     * @param string $role
     * @return Utilisateur
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return string 
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set urlImg
     *
     * @param string $urlImg
     * @return Utilisateur
     */
    public function setUrlImg($urlImg)
    {
        $this->urlImg = $urlImg;

        return $this;
    }

    /**
     * Get urlImg
     *
     * @return string 
     */
    public function getUrlImg()
    {
        return $this->urlImg;
    }
    
    /**
     * Set bloque
     *
     * @param integer $bloque
     * @return Utilisateur
     */
    public function setBloque($bloque)
    {
        $this->bloque = $bloque;

        return $this;
    }

    /**
     * Get bloque
     *
     * @return integer 
     */
    public function getBloque()
    {
        return $this->bloque;
    }
}
