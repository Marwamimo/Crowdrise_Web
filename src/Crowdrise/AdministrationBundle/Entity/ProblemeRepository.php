<?php

namespace Crowdrise\AdministrationBundle\Entity;

use Doctrine\ORM\EntityRepository;

class ProblemeRepository extends EntityRepository{
    
     public function CountNBProblemes(){
         
          $query=$this->getEntityManager()
                 
                  ->createQuery('SELECT COUNT(p.idProbleme) FROM CrowdriseAdministrationBundle:Probleme p');
     
      return $result = $query->getSingleScalarResult();


    }
}
