<?php

namespace Crowdrise\AdministrationBundle\Entity;

use Doctrine\ORM\EntityRepository;

class ProjetRepository extends EntityRepository{
    
    public function CountNBProjects(){
         
          $query=$this->getEntityManager()
                 
                  ->createQuery('SELECT COUNT(p.idProjet) FROM CrowdriseAdministrationBundle:Projet p');
     
      return $result = $query->getSingleScalarResult();


    }
}
