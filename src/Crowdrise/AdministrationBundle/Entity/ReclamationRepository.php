<?php

namespace Crowdrise\AdministrationBundle\Entity;

use Doctrine\ORM\EntityRepository;

class ReclamationRepository extends EntityRepository{
 
    public function CountNBClaims(){
         
          $query=$this->getEntityManager()
                 
                  ->createQuery('SELECT COUNT(r.idReclamation) FROM CrowdriseAdministrationBundle:Reclamation r');
     
      return $result = $query->getSingleScalarResult();


    }
}
