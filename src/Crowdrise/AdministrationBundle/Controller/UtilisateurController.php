<?php

namespace Crowdrise\AdministrationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Crowdrise\AdministrationBundle\Entity\Utilisateur;


class UtilisateurController extends Controller{
    
    public function affichageUtilisateursAction(){
        
        $em = $this->getDoctrine()->getManager();
        $utilisateurs = $em->getRepository("CrowdriseAdministrationBundle:Utilisateur")->findAll();
        
        return $this->render('CrowdriseAdministrationBundle:Default:users.html.twig', array("utilisateurs"=>$utilisateurs));
    }
    
    public function suppUtilisateurAction($id)
    {
         $em = $this->getDoctrine()->getManager();
         $utilisateur = $em->getRepository('CrowdriseAdministrationBundle:Utilisateur')->find($id);
         $em->remove($utilisateur);
         $em->flush();
         return $this->redirect($this->generateUrl('crowdrise_administration_users'));

    }
    
    public function blockUtilisateurAction($id)
    {
     
        $em = $this->getDoctrine()->getManager();
         
        $block = $em->getRepository("CrowdriseAdministrationBundle:Utilisateur")->blockUser($id);
        return $this->redirect($this->generateUrl('crowdrise_administration_users'));

    }
    
}
