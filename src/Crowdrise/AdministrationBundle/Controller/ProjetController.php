<?php

namespace Crowdrise\AdministrationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Crowdrise\AdministrationBundle\Entity\Projet;

class ProjetController extends Controller{
    
    public function affichageProjetsAction(){
        
        $em = $this->getDoctrine()->getManager();
        $projets = $em->getRepository("CrowdriseAdministrationBundle:Projet")->findAll();
        
        return $this->render('CrowdriseAdministrationBundle:Default:projects.html.twig', array("projets"=>$projets));
    }
    
   public function suppProjetAction($id)
    {
         $em = $this->getDoctrine()->getManager();
         $projet = $em->getRepository('CrowdriseAdministrationBundle:Projet')->find($id);
         $em->remove($projet);
         $em->flush();
         return $this->redirect($this->generateUrl('crowdrise_administration_projects'));

    }
}
