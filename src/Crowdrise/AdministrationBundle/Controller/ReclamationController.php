<?php

namespace Crowdrise\AdministrationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Crowdrise\AdministrationBundle\Entity\Reclamation;

class ReclamationController extends Controller {
    
   
    public function affichageReclamationsAction(){
        
        $em = $this->getDoctrine()->getManager();
        $reclamations = $em->getRepository("CrowdriseAdministrationBundle:Reclamation")->findAll();
        
        return $this->render('CrowdriseAdministrationBundle:Default:claims.html.twig', array("reclamations"=>$reclamations));
    }
    
   public function suppReclamationAction($id)
    {
         $em = $this->getDoctrine()->getManager();
         $reclamation = $em->getRepository('CrowdriseAdministrationBundle:Reclamation')->find($id);
         $em->remove($reclamation);
         $em->flush();
         return $this->redirect($this->generateUrl('crowdrise_administration_claims'));

    }
}
