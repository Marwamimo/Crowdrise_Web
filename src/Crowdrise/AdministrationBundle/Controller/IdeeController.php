<?php

namespace Crowdrise\AdministrationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Crowdrise\AdministrationBundle\Entity\Idee;

class IdeeController extends Controller{
    
    
    public function affichageIdeesAction(){
        
        $em = $this->getDoctrine()->getManager();
        $idees = $em->getRepository("CrowdriseAdministrationBundle:Idee")->findBy(array('etatIdee' => 'En_Attente'));
        
        return $this->render('CrowdriseAdministrationBundle:Default:ideas_requests.html.twig', array("idees"=>$idees));
    }
    
   public function acceptIdeeAction($id)
    {
         $em = $this->getDoctrine()->getManager();
         
        $accp = $em->getRepository("CrowdriseAdministrationBundle:Idee")->AcceptIdea($id);
        return $this->redirect($this->generateUrl('crowdrise_administration_ideas_requests'));
    }
    
    public function RefuseIdeaAction($id)
    {
         $em = $this->getDoctrine()->getManager();
         
        $accp = $em->getRepository("CrowdriseAdministrationBundle:Idee")->RefuseIdea($id);
        return $this->redirect($this->generateUrl('crowdrise_administration_ideas_requests'));
    }
}
