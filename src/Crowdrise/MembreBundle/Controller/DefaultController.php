<?php

namespace Crowdrise\MembreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('CrowdriseMembreBundle:Default:index.html.twig');
    }
    
    public function loginAction()
    {
        return $this->render('CrowdriseMembreBundle:Default:login.html.twig');
    }
    
    public function registerAction()
    {
        return $this->render('CrowdriseMembreBundle:Default:register.html.twig');
    }
}
